inherited frmUser: TfrmUser
  Tag = 3
  Width = 973
  Height = 714
  ExplicitWidth = 973
  ExplicitHeight = 714
  object LayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 973
    Height = 714
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = frmData.LayoutSkinLookAndFeel
    object btn_Refresh: TcxButton
      Left = 10
      Top = 10
      Width = 81
      Height = 25
      Action = Action_Refresh
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 0
    end
    object btn_Add: TcxButton
      Left = 109
      Top = 10
      Width = 75
      Height = 25
      Action = Action_Add
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 1
    end
    object btn_Save: TcxButton
      Left = 190
      Top = 10
      Width = 91
      Height = 25
      Action = Action_Save
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
    end
    object btn_Configuration: TcxButton
      Left = 299
      Top = 10
      Width = 97
      Height = 25
      Action = Action_Configuration
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 3
    end
    object grd_User: TcxGrid
      Left = 10
      Top = 53
      Width = 134
      Height = 645
      TabOrder = 4
      object grd_UserDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = frmData.ImageList_24x24
        Navigator.Buttons.First.ImageIndex = 93
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.ImageIndex = 91
        Navigator.Buttons.Next.ImageIndex = 92
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.ImageIndex = 94
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.ImageIndex = 4
        Navigator.Buttons.Refresh.Visible = True
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.ImageIndex = 6
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.Layout = fplCompact
        FindPanel.Location = fplGroupByBox
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DS_USER
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        object grd_UserDBTableViewID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          VisibleForCustomization = False
        end
        object grd_UserDBTableViewNAME: TcxGridDBColumn
          Caption = 'Nom'
          DataBinding.FieldName = 'NAME'
          HeaderAlignmentHorz = taCenter
          Width = 107
        end
        object grd_UserDBTableViewFIRSTNAME: TcxGridDBColumn
          Caption = 'Pr'#233'nom'
          DataBinding.FieldName = 'FIRSTNAME'
          HeaderAlignmentHorz = taCenter
          Width = 129
        end
        object grd_UserDBTableViewSMARTPHONE: TcxGridDBColumn
          Caption = 'T'#233'l'#233'phone Portable'
          DataBinding.FieldName = 'SMARTPHONE'
          Visible = False
        end
        object grd_UserDBTableViewPHONE: TcxGridDBColumn
          Caption = 'T'#233'l'#233'phone Fixe'
          DataBinding.FieldName = 'PHONE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_UserDBTableViewEMAIL: TcxGridDBColumn
          Caption = 'Email'
          DataBinding.FieldName = 'EMAIL'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_UserDBTableViewFULLNAME: TcxGridDBColumn
          DataBinding.FieldName = 'FULLNAME'
          Visible = False
          VisibleForCustomization = False
        end
        object grd_UserDBTableViewID_JOB: TcxGridDBColumn
          Caption = 'Fonction'
          DataBinding.FieldName = 'ID_JOB'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_UserDBTableViewID_CREATOR: TcxGridDBColumn
          Caption = 'Cr'#233'ateur'
          DataBinding.FieldName = 'ID_CREATOR'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'FULLNAME'
            end>
          Properties.ListSource = DS_CREATEUR
          Visible = False
        end
        object grd_UserDBTableViewDATE_CREATION: TcxGridDBColumn
          Caption = 'Date de cr'#233'ation'
          DataBinding.FieldName = 'DATE_CREATION'
          PropertiesClassName = 'TcxDateEditProperties'
          Visible = False
        end
        object grd_UserDBTableViewPERSONAL_SMARTPHONE: TcxGridDBColumn
          Caption = 'T'#233'l'#233'phone Personnel'
          DataBinding.FieldName = 'PERSONAL_SMARTPHONE'
          Visible = False
        end
        object grd_UserDBTableViewPERSONAL_EMAIL: TcxGridDBColumn
          Caption = 'Email personnel'
          DataBinding.FieldName = 'PERSONAL_EMAIL'
          Visible = False
        end
      end
      object grd_UserLevel: TcxGridLevel
        GridView = grd_UserDBTableView
      end
    end
    object grd_PDF: TcxGrid
      Left = 827
      Top = 127
      Width = 121
      Height = 553
      TabOrder = 16
      object grd_PDFDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
      end
      object grd_PDFLevel: TcxGridLevel
        GridView = grd_PDFDBTableView
      end
    end
    object btn_PDF: TcxButton
      Left = 827
      Top = 96
      Width = 75
      Height = 25
      Caption = 'Exporter'
      OptionsImage.ImageIndex = 13
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 15
    end
    object txtedt_Name: TcxDBTextEdit
      Left = 191
      Top = 96
      DataBinding.DataField = 'NAME'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Width = 142
    end
    object txtedt_Firstname: TcxDBTextEdit
      Left = 380
      Top = 96
      DataBinding.DataField = 'FIRSTNAME'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
      Width = 145
    end
    object txtedt_Email: TcxDBTextEdit
      Left = 194
      Top = 121
      DataBinding.DataField = 'EMAIL'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 7
      Width = 331
    end
    object txtedt_Phone: TcxDBTextEdit
      Left = 211
      Top = 146
      DataBinding.DataField = 'PHONE'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 8
      Width = 121
    end
    object txtedt_Smatphone: TcxDBTextEdit
      Left = 404
      Top = 146
      DataBinding.DataField = 'SMARTPHONE'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 9
      Width = 121
    end
    object datedt_DateCreation: TcxDBDateEdit
      Left = 646
      Top = 121
      DataBinding.DataField = 'DATE_CREATION'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 12
      Width = 145
    end
    object lkpcmb_Createur: TcxDBLookupComboBox
      Left = 646
      Top = 96
      DataBinding.DataField = 'ID_CREATOR'
      DataBinding.DataSource = DS_USER
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'FULLNAME'
        end>
      Properties.ListSource = DS_CREATEUR
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      Width = 145
    end
    object txtedt_PersonalPhone: TcxDBTextEdit
      Left = 646
      Top = 207
      DataBinding.DataField = 'PERSONAL_SMARTPHONE'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 13
      Width = 145
    end
    object txtedt_PersonalEmail: TcxDBTextEdit
      Left = 646
      Top = 232
      DataBinding.DataField = 'PERSONAL_EMAIL'
      DataBinding.DataSource = DS_USER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 14
      Width = 145
    end
    object lkpcmb_Job: TcxDBLookupComboBox
      Left = 211
      Top = 171
      DataBinding.DataField = 'ID_JOB'
      DataBinding.DataSource = DS_USER
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'LABEL'
        end>
      Properties.ListOptions.ShowHeader = False
      Properties.ListSource = DS_JOB
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 10
      Width = 122
    end
    object LayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 2
      ShowBorder = False
      Index = -1
    end
    object LayoutItem_btn_Refresh: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Refresh
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 81
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_btn_Add: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Add
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutItem_btn_Update: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Save
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 91
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object LayoutItem_btn_Option: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Configuration
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 97
      ControlOptions.ShowBorder = False
      Index = 5
    end
    object LayoutSeparatorItem1: TdxLayoutSeparatorItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutSeparatorItem2: TdxLayoutSeparatorItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Separator'
      Index = 4
    end
    object LayoutGroup_Navigation: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 4
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Body: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutGroup_grd_User: TdxLayoutGroup
      Parent = LayoutGroup_Body
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_data_user: TdxLayoutGroup
      Parent = LayoutGroup_Body
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 1
    end
    object LayoutGroup_grd_pdf: TdxLayoutGroup
      Parent = LayoutGroup_Body
      AlignHorz = ahClient
      CaptionOptions.Text = 'PDF'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 2
    end
    object LayoutItem_grd_User: TdxLayoutItem
      Parent = LayoutGroup_grd_User
      AlignHorz = ahClient
      AlignVert = avClient
      Control = grd_User
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_btn_ImportPDF: TdxLayoutItem
      Parent = LayoutGroup_grd_pdf
      AlignHorz = ahLeft
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_PDF
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_grd_PDF: TdxLayoutItem
      Parent = LayoutGroup_grd_pdf
      AlignVert = avClient
      Control = grd_PDF
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object dxLayoutItem2: TdxLayoutItem
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'cxButton2'
      CaptionOptions.Visible = False
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object dxLayoutSeparatorItem1: TdxLayoutSeparatorItem
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutGroup_Body_Informations: TdxLayoutGroup
      Parent = LayoutGroup_data_user
      AlignHorz = ahCenter
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Information_Pro: TdxLayoutGroup
      Parent = LayoutGroup_Body_Informations
      CaptionOptions.Text = 'Informations Professionnelles'
      ButtonOptions.Buttons = <>
      ItemIndex = 3
      Index = 0
    end
    object LayoutGroup_Creation_Per: TdxLayoutGroup
      Parent = LayoutGroup_Body_Informations
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      ShowBorder = False
      Index = 1
    end
    object LayoutGroup_Creation: TdxLayoutGroup
      Parent = LayoutGroup_Creation_Per
      CaptionOptions.Text = 'Cr'#233'ateur'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 0
    end
    object LayoutGroup_Personal: TdxLayoutGroup
      Parent = LayoutGroup_Creation_Per
      CaptionOptions.Text = 'Informations personnelles'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 1
    end
    object LayoutItem_txtedt_Name: TdxLayoutItem
      Parent = LayoutGroup_InfoPro_Line1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Nom'
      Control = txtedt_Name
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_FistName: TdxLayoutItem
      Parent = LayoutGroup_InfoPro_Line1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Pr'#233'nom'
      Control = txtedt_Firstname
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_Fixe: TdxLayoutItem
      Parent = LayoutGroup_InfoPro_Line2
      AlignHorz = ahClient
      CaptionOptions.Text = 'T'#233'l. Fixe'
      Control = txtedt_Phone
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_Smartphone: TdxLayoutItem
      Parent = LayoutGroup_InfoPro_Line2
      AlignHorz = ahClient
      CaptionOptions.Text = 'T'#233'l. Portable'
      Control = txtedt_Smatphone
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_lkpcmb_Job: TdxLayoutItem
      Parent = LayoutGroup_Information_Pro
      CaptionOptions.Text = 'Email'
      Control = txtedt_Email
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_Email: TdxLayoutItem
      Parent = LayoutGroup_Information_Pro
      AlignHorz = ahLeft
      CaptionOptions.Text = 'Fonction'
      Control = lkpcmb_Job
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 122
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object LayoutGroup_InfoPro_Line1: TdxLayoutGroup
      Parent = LayoutGroup_Information_Pro
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_InfoPro_Line2: TdxLayoutGroup
      Parent = LayoutGroup_Information_Pro
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutItem_lkpcmb_Creator: TdxLayoutItem
      Parent = LayoutGroup_Creation
      CaptionOptions.Text = 'Cr'#233'ateur'
      Control = lkpcmb_Createur
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_datedt_DateCreation: TdxLayoutItem
      Parent = LayoutGroup_Creation
      CaptionOptions.Text = 'Date de cr'#233'ation'
      Control = datedt_DateCreation
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_MailPerso: TdxLayoutItem
      Parent = LayoutGroup_Personal
      CaptionOptions.Text = 'Email'
      Control = txtedt_PersonalEmail
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_NumPerso: TdxLayoutItem
      Parent = LayoutGroup_Personal
      CaptionOptions.Text = 'T'#233'l'#233'phone'
      Control = txtedt_PersonalPhone
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object DS_USER: TDataSource
    DataSet = frmData.FD_USER
    Left = 16
    Top = 160
  end
  object DS_JOB: TDataSource
    DataSet = frmData.FD_JOB
    Left = 44
    Top = 160
  end
  object DS_CREATEUR: TDataSource
    DataSet = frmData.FD_CREATEUR
    Left = 72
    Top = 160
  end
  object ActionList_Navigation: TActionList
    Images = frmData.ImageList_24x24
    Left = 16
    Top = 208
    object Action_Refresh: TAction
      Caption = 'Rafraichir'
      ImageIndex = 4
      OnExecute = Action_RefreshExecute
    end
    object Action_Add: TAction
      Caption = 'Ajouter'
      ImageIndex = 0
      OnExecute = Action_AddExecute
    end
    object Action_Save: TAction
      Caption = 'Sauvegarder'
      ImageIndex = 73
      OnExecute = Action_SaveExecute
    end
    object Action_Configuration: TAction
      Caption = 'Configuration'
      ImageIndex = 14
      OnExecute = Action_ConfigurationExecute
    end
  end
end
