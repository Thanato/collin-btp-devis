unit EMPDevisDevis;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, EMPDevisFrame, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, BDLSkin,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxLayoutContainer, cxClasses, dxLayoutControl, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, dxLayoutControlAdapters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations,
  Data.DB, cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxCalendar, cxCurrencyEdit,
  cxContainer, cxTextEdit, cxDBEdit, dxLayoutcxEditAdapters, cxDropDownEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxMaskEdit, System.Actions,
  Vcl.ActnList, Vcl.ComCtrls, dxCore, cxDateUtils, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxTLData, cxDBTL;

type
  TfrmDevis = class(TfrmFrame)
    LayoutControlGroup_Root: TdxLayoutGroup;
    LayoutControl: TdxLayoutControl;
    LayoutGroup_Navigation: TdxLayoutGroup;
    LayoutGroup_Body: TdxLayoutGroup;
    LayoutGroup_GridDevis: TdxLayoutGroup;
    LayoutGroup_BodyDevis: TdxLayoutGroup;
    LayoutGroup_GridPDF: TdxLayoutGroup;
    LayoutItem_grd_Devis: TdxLayoutItem;
    LayoutItem_grd_PDF: TdxLayoutItem;
    LayoutGroup_TabDevis: TdxLayoutGroup;
    LayoutGroup_Devis_CustomerFolder: TdxLayoutGroup;
    LayoutGroup_Devis_Etude: TdxLayoutGroup;
    LayoutGroup_PiedAffaire: TdxLayoutGroup;
    LayoutGroup_Debourse: TdxLayoutGroup;
    LayoutSeparatorItem: TdxLayoutSeparatorItem;
    LayoutItem_btn_Add: TdxLayoutItem;
    LayoutItem_btn_Update: TdxLayoutItem;
    LayoutSeparatorItem1: TdxLayoutSeparatorItem;
    LayoutItem_btn_Printer: TdxLayoutItem;
    LayoutItem_btn_ExportPDF: TdxLayoutItem;
    LayoutItem_btn_EportExel: TdxLayoutItem;
    LayoutItem_btn_ImportPDF: TdxLayoutItem;
    LayoutGroup_PDF_Navigation: TdxLayoutGroup;
    btn_ExportXLS: TcxButton;
    btn_ExportPDF: TcxButton;
    btn_Print: TcxButton;
    btn_Update: TcxButton;
    btn_Ajouter: TcxButton;
    grd_DevisDBTableView: TcxGridDBTableView;
    grd_DevisLevel: TcxGridLevel;
    grd_Devis: TcxGrid;
    btn_ImportPDF: TcxButton;
    grd_PDFDBTableView: TcxGridDBTableView;
    grd_PDFLevel: TcxGridLevel;
    grd_PDF: TcxGrid;
    LayoutItem_btn_Bilan: TdxLayoutItem;
    LayoutSeparatorItem2: TdxLayoutSeparatorItem;
    LayoutSplitterItem1: TdxLayoutSplitterItem;
    LayoutSplitterItem2: TdxLayoutSplitterItem;
    btn_Bilan: TcxButton;
    LayoutGroup_Devis_DataDevis: TdxLayoutGroup;
    LayoutGroup_Devis_DataLiaisons: TdxLayoutGroup;
    LayoutGroup_Devis_CustomerFolder1: TdxLayoutGroup;
    DS_INVOICE: TDataSource;
    grd_DevisDBTableViewID: TcxGridDBColumn;
    grd_DevisDBTableViewDIRECTORY_NUMBER: TcxGridDBColumn;
    grd_DevisDBTableViewREFERENCE: TcxGridDBColumn;
    grd_DevisDBTableViewID_CUSTOMER: TcxGridDBColumn;
    grd_DevisDBTableViewID_BUILDING_SITE: TcxGridDBColumn;
    grd_DevisDBTableViewLABEL: TcxGridDBColumn;
    grd_DevisDBTableViewCREATION_DATE: TcxGridDBColumn;
    grd_DevisDBTableViewMODIFICATION_DATE: TcxGridDBColumn;
    grd_DevisDBTableViewID_CREATOR: TcxGridDBColumn;
    grd_DevisDBTableViewID_MODIFIER: TcxGridDBColumn;
    grd_DevisDBTableViewID_STATE: TcxGridDBColumn;
    grd_DevisDBTableViewID_CATEGORY: TcxGridDBColumn;
    grd_DevisDBTableViewID_TVA: TcxGridDBColumn;
    grd_DevisDBTableViewTTC_AMOUNT: TcxGridDBColumn;
    grd_DevisDBTableViewHT_AMOUNT: TcxGridDBColumn;
    grd_DevisDBTableViewID_HOLDBACK: TcxGridDBColumn;
    LayoutItem_btn_Refresh: TdxLayoutItem;
    LayoutItem_btn_Paste: TdxLayoutItem;
    dxLayoutSeparatorItem1: TdxLayoutSeparatorItem;
    btn_Refresh: TcxButton;
    btn_Paste: TcxButton;
    LayoutGroup_Devis_CustomerFolder2: TdxLayoutGroup;
    LayoutItem_txtedt_Reference: TdxLayoutItem;
    LayoutItem_datedt_DateCreation: TdxLayoutItem;
    LayoutItem_txtedt_Libelle: TdxLayoutItem;
    LayoutItem_lkpcmb_TVA: TdxLayoutItem;
    LayoutItem_lkpcmb_Etat: TdxLayoutItem;
    LayoutItem_lkpcmb_Category: TdxLayoutItem;
    LayoutGroup_FolderCustomer_Informations_line3: TdxLayoutGroup;
    LayoutGroup_FolderCustomer_Informations_line2: TdxLayoutGroup;
    LayoutGroup_FolderCustomer_Informations_line1: TdxLayoutGroup;
    txtedt_Reference: TcxDBTextEdit;
    datedt_DateCreation: TcxDBDateEdit;
    txtedt_Libelle: TcxDBTextEdit;
    lkpcmb_Etat: TcxDBLookupComboBox;
    lkpcmb_TVA: TcxDBLookupComboBox;
    lkpcmb_Categorie: TcxDBLookupComboBox;
    LayoutItem_txtedt_Dossier: TdxLayoutItem;
    LayoutItem_txtedt_AffaireAssoci�: TdxLayoutItem;
    LayoutItem_lkpcmb_Chantier: TdxLayoutItem;
    LayoutGroup_Devis_Client: TdxLayoutGroup;
    LayoutGroup_Devis_Createur: TdxLayoutGroup;
    DS_USER: TDataSource;
    DS_CUSTOMER: TDataSource;
    ActionList_Navigation: TActionList;
    Action_Refresh: TAction;
    Action_Add: TAction;
    Action_Update: TAction;
    Action_Paste: TAction;
    LayoutItem_lkpcmb_Customer: TdxLayoutItem;
    LayoutItem_cmb_Condition: TdxLayoutItem;
    LayoutItem_txtedt_Portable: TdxLayoutItem;
    LayoutItem_txtedt_Fixe: TdxLayoutItem;
    LayoutItem_txtedt_Contact: TdxLayoutItem;
    LayoutItem_txtedt_Email: TdxLayoutItem;
    LayoutItem_lkpcmb_Creator: TdxLayoutItem;
    LayoutItem_lkpcmb_Modifier: TdxLayoutItem;
    LayoutItem_datedt_ModificationDate: TdxLayoutItem;
    LayoutGroup_Devis_Client_Line1: TdxLayoutGroup;
    LayoutGroup_Devis_Client_Line3: TdxLayoutGroup;
    LayoutGroup_Devis_Client_Line2: TdxLayoutGroup;
    lkpcmb_Cr�ateur: TcxDBLookupComboBox;
    lkpcmb_Modifier: TcxDBLookupComboBox;
    datedt_Modification: TcxDBDateEdit;
    lkpcmb_AffaireAssocie: TcxDBLookupComboBox;
    lkpcmb_ConditionReglement: TcxDBLookupComboBox;
    lkpcmb_Client: TcxDBLookupComboBox;
    lkpcmb_Chantier: TcxDBLookupComboBox;
    lkpcmb_Dossier: TcxDBLookupComboBox;
    txtedt_Contact: TcxDBTextEdit;
    txtedt_TelFixe: TcxDBTextEdit;
    txtedt_TelPortable: TcxDBTextEdit;
    txtedt_Email: TcxDBTextEdit;
    ActionBilan: TAction;
    cxDBTreeList1: TcxDBTreeList;
    procedure Action_RefreshExecute(Sender: TObject);
    procedure Action_AddExecute(Sender: TObject);
    procedure Action_UpdateExecute(Sender: TObject);
    procedure Action_PasteExecute(Sender: TObject);
    procedure DS_INVOICEDataChange(Sender: TObject; Field: TField);
    procedure ActionBilanExecute(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    constructor Create(AOwner: TComponent); override;
  end;

var
  frmDevis: TfrmDevis;

implementation

uses
  EMPDevisData, EMPDevisInvoiceBilan;

{$R *.dfm}

procedure TfrmDevis.ActionBilanExecute(Sender: TObject);
begin
  inherited;
  CreationInvoiceBilan();
end;

procedure TfrmDevis.Action_AddExecute(Sender: TObject);
var
  id_invoice : integer;
begin
  inherited;

  try
     id_invoice := frmData.InvoiceAdd('1');
    frmData.FDMemGetTables('FD_INVOICE');
    frmData.FD_INVOICE.Locate('ID', id_invoice, [loCaseInsensitive]);

    ShowMessage('Nouveau devis cr��.');
  except on E: Exception do
    ShowMessage('Une erreur est survenue lors de la cr�ation du devis.');
  end;
end;

procedure TfrmDevis.Action_PasteExecute(Sender: TObject);
var
  id_invoice : integer;
  paste_reference : string;
begin
  inherited;

  try
    paste_reference := frmData.FD_INVOICEREFERENCE.AsString;
    id_invoice := frmData.InvoicePaste(frmData.FD_INVOICEID.AsString + ';1');
    frmData.FDMemGetTables('FD_INVOICE');
    frmData.FD_INVOICE.Locate('ID', id_invoice, [loCaseInsensitive]);
    
    ShowMessage('La copie du devis ' + paste_reference + ' a �t� effectu�e');
  except on E: Exception do
    ShowMessage('Une erreur est survenue lors de la copie du devis ' + paste_reference + '.');
  end;
end;

procedure TfrmDevis.Action_RefreshExecute(Sender: TObject);
begin
  inherited;

  frmData.FDMemGetTables('FD_INVOICE');
end;

procedure TfrmDevis.Action_UpdateExecute(Sender: TObject);
begin
  inherited;

  if (frmData.FD_INVOICE.State = dsEdit) or (frmData.FD_INVOICE.State = dsInsert) then
  begin
    try
      frmData.FD_INVOICEID_MODIFIER.AsInteger := 1;
      frmData.FD_INVOICEMODIFICATION_DATE.AsDateTime := Date();

      frmData.FD_INVOICE.Post;
      frmData.FDMemUpdate('FD_INVOICE');

      ShowMessage('Devis ' + frmData.FD_INVOICEREFERENCE.AsString + ' a �t� sauvegard�.');
    except on E: Exception do
      Showmessage('Une erreur est survenue lors de l''enregistrement du devis ' + frmData.FD_INVOICEREFERENCE.AsString + '.');
    end;

  end
  else
  begin
    ShowMessage('Aucunes modifications apport�es.');
  end;

end;

Constructor TfrmDevis.Create(AOwner: TComponent);
begin
  inherited;

  frmData.FDMemGetTables('FD_INVOICE;FD_CUSTOMER');
end;

procedure TfrmDevis.DS_INVOICEDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  if self.Showing then
  begin
    if frmData.FD_INVOICEID_CUSTOMER.AsString <> '' then
    begin
      frmData.FD_CUSTOMER.Active := true;
      frmData.FDMemGetTables('FD_CUSTOMER');
      frmdata.FD_CUSTOMER.Locate('ID', frmData.FD_INVOICEID_CUSTOMER.AsInteger, [loCaseinsensitive]);
    end
    else
    begin
      frmData.FD_CUSTOMER.Active := false;
    end;
  end;
end;

initialization
RegisterFrame(IDDevis, TfrmDevis);

end.
