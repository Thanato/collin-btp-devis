inherited frmCustomer: TfrmCustomer
  Tag = 3
  Width = 1139
  Height = 765
  ExplicitWidth = 1139
  ExplicitHeight = 765
  object LayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1139
    Height = 765
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = frmData.LayoutSkinLookAndFeel
    ExplicitLeft = -158
    ExplicitTop = -148
    ExplicitWidth = 834
    ExplicitHeight = 642
    object btn_Refresh: TcxButton
      Left = 10
      Top = 10
      Width = 75
      Height = 25
      Action = ActionRefresh
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 0
    end
    object btn_Add: TcxButton
      Left = 103
      Top = 10
      Width = 75
      Height = 25
      Action = ActionAdd
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 1
    end
    object btn_Save: TcxButton
      Left = 184
      Top = 10
      Width = 97
      Height = 25
      Action = ActionSave
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
    end
    object grd_Customer: TcxGrid
      Left = 10
      Top = 53
      Width = 250
      Height = 685
      TabOrder = 3
      object grd_CustomerDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = frmData.ImageList_24x24
        Navigator.Buttons.First.ImageIndex = 93
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.ImageIndex = 91
        Navigator.Buttons.Next.ImageIndex = 92
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.ImageIndex = 94
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.ImageIndex = 4
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.ImageIndex = 6
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.Layout = fplCompact
        FindPanel.Location = fplGroupByBox
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DS_CUSTOMER
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        object grd_CustomerDBTableViewID: TcxGridDBColumn
          Caption = 'N'#176' Client'
          DataBinding.FieldName = 'ID'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_CustomerDBTableViewNAME: TcxGridDBColumn
          Caption = 'Nom'
          DataBinding.FieldName = 'NAME'
          HeaderAlignmentHorz = taCenter
          Width = 148
        end
        object grd_CustomerDBTableViewPRIVATE: TcxGridDBColumn
          Caption = 'Priv'#233
          DataBinding.FieldName = 'PRIVATE'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.Alignment = taCenter
          Properties.DisplayChecked = '1'
          Properties.DisplayUnchecked = '0'
          Properties.NullStyle = nssUnchecked
          Properties.ValueChecked = '1'
          Properties.ValueUnchecked = '0'
          HeaderAlignmentHorz = taCenter
        end
        object grd_CustomerDBTableViewCONDITION: TcxGridDBColumn
          Caption = 'Condition'
          DataBinding.FieldName = 'CONDITION'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 219
        end
        object grd_CustomerDBTableViewPHONE: TcxGridDBColumn
          Caption = 'T'#233'l. Fixe'
          DataBinding.FieldName = 'PHONE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_CustomerDBTableViewSMARTPHONE: TcxGridDBColumn
          Caption = 'T'#233'l. Portable'
          DataBinding.FieldName = 'SMARTPHONE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_CustomerDBTableViewEMAIL: TcxGridDBColumn
          Caption = 'Email'
          DataBinding.FieldName = 'EMAIL'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 175
        end
        object grd_CustomerDBTableViewADDRESS: TcxGridDBColumn
          Caption = 'Adresse'
          DataBinding.FieldName = 'ADDRESS'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 190
        end
        object grd_CustomerDBTableViewPOSTAL_CODE: TcxGridDBColumn
          Caption = 'Code Postal'
          DataBinding.FieldName = 'POSTAL_CODE'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 81
        end
        object grd_CustomerDBTableViewCITY: TcxGridDBColumn
          Caption = 'Ville'
          DataBinding.FieldName = 'CITY'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_CustomerDBTableViewCONTACT: TcxGridDBColumn
          Caption = 'Contact'
          DataBinding.FieldName = 'CONTACT'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 151
        end
        object grd_CustomerDBTableViewDATE_MODIFICATION: TcxGridDBColumn
          DataBinding.FieldName = 'DATE_MODIFICATION'
          Visible = False
          VisibleForCustomization = False
        end
        object grd_CustomerDBTableViewID_MODIFIER: TcxGridDBColumn
          DataBinding.FieldName = 'ID_MODIFIER'
          Visible = False
          VisibleForCustomization = False
        end
        object grd_CustomerDBTableViewDATE_CREATION: TcxGridDBColumn
          DataBinding.FieldName = 'DATE_CREATION'
          Visible = False
          VisibleForCustomization = False
        end
        object grd_CustomerDBTableViewID_CREATOR: TcxGridDBColumn
          DataBinding.FieldName = 'ID_CREATOR'
          Visible = False
          VisibleForCustomization = False
        end
      end
      object grd_CustomerLevel: TcxGridLevel
        GridView = grd_CustomerDBTableView
      end
    end
    object grd_PDF: TcxGrid
      Left = 1027
      Top = 127
      Width = 250
      Height = 593
      TabOrder = 19
      object grd_PDFDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
      end
      object grd_PDFLevel: TcxGridLevel
        GridView = grd_PDFDBTableView
      end
    end
    object btn_PDF: TcxButton
      Left = 1027
      Top = 96
      Width = 75
      Height = 25
      Caption = 'Exporter'
      OptionsImage.ImageIndex = 13
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 18
    end
    object txtedt_Name: TcxDBTextEdit
      Left = 343
      Top = 96
      DataBinding.DataField = 'NAME'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 4
      Width = 121
    end
    object chkbox_Private: TcxDBCheckBox
      Left = 714
      Top = 96
      DataBinding.DataField = 'PRIVATE'
      DataBinding.DataSource = DS_CUSTOMER
      Properties.DisplayChecked = '1'
      Properties.DisplayUnchecked = '0'
      Properties.NullStyle = nssUnchecked
      Properties.ValueChecked = '1'
      Properties.ValueUnchecked = '0'
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 6
    end
    object cmb_ConditionReglement: TcxDBComboBox
      Left = 558
      Top = 96
      DataBinding.DataField = 'CONDITION'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 5
      Width = 121
    end
    object txtedt_Adresse: TcxDBTextEdit
      Left = 343
      Top = 122
      DataBinding.DataField = 'ADDRESS'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 7
      Width = 387
    end
    object txtedt_Ville: TcxDBTextEdit
      Left = 343
      Top = 147
      DataBinding.DataField = 'CITY'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 8
      Width = 198
    end
    object txtedt_CodePostal: TcxDBTextEdit
      Left = 609
      Top = 147
      DataBinding.DataField = 'POSTAL_CODE'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 9
      Width = 121
    end
    object txtedt_NomContact: TcxDBTextEdit
      Left = 343
      Top = 172
      DataBinding.DataField = 'CONTACT'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 10
      Width = 121
    end
    object txtedt_Email: TcxDBTextEdit
      Left = 499
      Top = 172
      DataBinding.DataField = 'EMAIL'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      Width = 231
    end
    object txtedt_Phone: TcxDBTextEdit
      Left = 343
      Top = 197
      DataBinding.DataField = 'PHONE'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 12
      Width = 155
    end
    object txtedt_Smatphone: TcxDBTextEdit
      Left = 570
      Top = 197
      DataBinding.DataField = 'SMARTPHONE'
      DataBinding.DataSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 13
      Width = 160
    end
    object lkpcmb_Creator: TcxDBLookupComboBox
      Left = 830
      Top = 96
      DataBinding.DataField = 'ID_CREATOR'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'FULLNAME'
        end>
      Properties.ListSource = DS_CREATEUR
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 14
      Width = 145
    end
    object lkpcmb_Modifier: TcxDBLookupComboBox
      Left = 830
      Top = 146
      DataBinding.DataField = 'ID_MODIFIER'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'FULLNAME'
        end>
      Properties.ListSource = DS_CREATEUR
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 16
      Width = 145
    end
    object datedt_DateCreation: TcxDBDateEdit
      Left = 830
      Top = 121
      DataBinding.DataField = 'DATE_CREATION'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 15
      Width = 145
    end
    object datedt_DateModification: TcxDBDateEdit
      Left = 830
      Top = 171
      DataBinding.DataField = 'DATE_MODIFICATION'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 17
      Width = 145
    end
    object LayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 2
      ShowBorder = False
      Index = -1
    end
    object LayoutGroup_Navigation: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 3
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Body: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutItem_btn_Refresh: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Refresh
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutSeparatorItem: TdxLayoutSeparatorItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutItem_btn_Add: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Add
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutItem_btn_Save: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Save
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 97
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object LayoutGroup_grd_User: TdxLayoutGroup
      Parent = LayoutGroup_Body
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Informations: TdxLayoutGroup
      Parent = LayoutGroup_Body
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutGroup_PDF: TdxLayoutGroup
      Parent = LayoutGroup_Body
      CaptionOptions.Text = 'PDF'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 4
    end
    object LayoutSeparatorItem1: TdxLayoutSeparatorItem
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutItem_grd_User: TdxLayoutItem
      Parent = LayoutGroup_grd_User
      AlignVert = avClient
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = grd_Customer
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutGroup_PDF_Navigation: TdxLayoutGroup
      Parent = LayoutGroup_PDF
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_btn_Export: TdxLayoutItem
      Parent = LayoutGroup_PDF_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_PDF
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_grd_PDF: TdxLayoutItem
      Parent = LayoutGroup_PDF
      AlignVert = avClient
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = grd_PDF
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutGroup_InformationsClient: TdxLayoutGroup
      Parent = LayoutGroup_Informations2
      CaptionOptions.Text = 'Informations Client'
      ButtonOptions.Buttons = <>
      ItemIndex = 4
      Index = 0
    end
    object LayoutItem_txtedt_Name: TdxLayoutItem
      Parent = LayoutGroup_InformationClient_Line1
      CaptionOptions.Text = 'Nom'
      Control = txtedt_Name
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_chkbox_Private: TdxLayoutItem
      Parent = LayoutGroup_InformationClient_Line1
      AlignHorz = ahLeft
      CaptionOptions.Text = 'Priv'#233
      Control = chkbox_Private
      ControlOptions.OriginalHeight = 20
      ControlOptions.OriginalWidth = 16
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutGroup_InformationClient_Line1: TdxLayoutGroup
      Parent = LayoutGroup_InformationsClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_cmb_ConditionReglement: TdxLayoutItem
      Parent = LayoutGroup_InformationClient_Line1
      CaptionOptions.Text = 'Cond. R'#233'glement'
      Control = cmb_ConditionReglement
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutGroup_InformationsClient_Line2: TdxLayoutGroup
      Parent = LayoutGroup_InformationsClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutItem_txtedt_Smatphone: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient_Line4
      AlignHorz = ahClient
      CaptionOptions.Text = 'T'#233'l. Portable'
      Control = txtedt_Smatphone
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_Phone: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient_Line4
      AlignHorz = ahClient
      CaptionOptions.Text = 'T'#233'l. Fixe'
      Control = txtedt_Phone
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_Address: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient
      CaptionOptions.Text = 'Adresse'
      Control = txtedt_Adresse
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_Ville: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient_Line2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Ville'
      Control = txtedt_Ville
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_CP: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient_Line2
      CaptionOptions.Text = 'Code Postal'
      Control = txtedt_CodePostal
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutGroup_InformationsClient_Line3: TdxLayoutGroup
      Parent = LayoutGroup_InformationsClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 3
    end
    object LayoutItem_txtedt_NomContact: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient_Line3
      CaptionOptions.Text = 'Contact'
      Control = txtedt_NomContact
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_Email: TdxLayoutItem
      Parent = LayoutGroup_InformationsClient_Line3
      AlignHorz = ahClient
      CaptionOptions.Text = 'Email'
      Control = txtedt_Email
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutGroup_InformationsClient_Line4: TdxLayoutGroup
      Parent = LayoutGroup_InformationsClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 4
    end
    object LayoutGroup_Creator: TdxLayoutGroup
      Parent = LayoutGroup_Informations2
      CaptionOptions.Text = 'Cr'#233'ateur'
      ButtonOptions.Buttons = <>
      ItemIndex = 3
      Index = 1
    end
    object LayoutItem_lkpcmb_Creator: TdxLayoutItem
      Parent = LayoutGroup_Creator
      CaptionOptions.Text = 'Cr'#233'ateur'
      Control = lkpcmb_Creator
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 0
    end
    object LayoutItem_datedt_Creation: TdxLayoutItem
      Parent = LayoutGroup_Creator
      CaptionOptions.Text = 'Cr'#233'ation'
      Control = datedt_DateCreation
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 1
    end
    object LayoutSplitterItem1: TdxLayoutSplitterItem
      Parent = LayoutGroup_Body
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
    object LayoutSplitterItem2: TdxLayoutSplitterItem
      Parent = LayoutGroup_Body
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 3
    end
    object LayoutGroup_Informations2: TdxLayoutGroup
      Parent = LayoutGroup_Informations
      AlignHorz = ahCenter
      AlignVert = avTop
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_lkpcmb_Modifier: TdxLayoutItem
      Parent = LayoutGroup_Creator
      CaptionOptions.Text = 'Modificateur'
      Control = lkpcmb_Modifier
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 2
    end
    object LayoutItem_datedt_DateModification: TdxLayoutItem
      Parent = LayoutGroup_Creator
      CaptionOptions.Text = 'Modification'
      Control = datedt_DateModification
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 3
    end
  end
  object DS_CUSTOMER: TDataSource
    DataSet = frmData.FD_CUSTOMER
    Left = 24
    Top = 160
  end
  object DS_CREATEUR: TDataSource
    DataSet = frmData.FD_CREATEUR
    Left = 52
    Top = 160
  end
  object ActionList_Navigation: TActionList
    Images = frmData.ImageList_24x24
    Left = 80
    Top = 160
    object ActionRefresh: TAction
      Caption = 'Rafraichir'
      ImageIndex = 4
      OnExecute = ActionRefreshExecute
    end
    object ActionAdd: TAction
      Caption = 'Ajouter'
      ImageIndex = 0
      OnExecute = ActionAddExecute
    end
    object ActionSave: TAction
      Caption = 'Sauvegarder'
      ImageIndex = 73
      OnExecute = ActionSaveExecute
    end
  end
end
