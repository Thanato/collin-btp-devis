program EMPDevisServ;
{$APPTYPE GUI}

{$R *.dres}

uses
  Vcl.Forms,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  EMPServMain in 'EMPServMain.pas' {frmServeur},
  ServerMethodsUnit in 'ServerMethodsUnit.pas' {ServerMethods: TDataModule},
  WebModuleUnit in 'WebModuleUnit.pas' {EMPWebModule: TWebModule};

{$R *.res}

begin
  if WebRequestHandler <> nil then
    WebRequestHandler.WebModuleClass := WebModuleClass;
  Application.Initialize;
  Application.CreateForm(TfrmServeur, frmServeur);
  Application.Run;
end.
