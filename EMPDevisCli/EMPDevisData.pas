unit EMPDevisData;

interface

uses
  System.SysUtils, System.Classes, dxSkinsCore, dxSkinBasic, BDLSkin,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  cxClasses, dxLayoutLookAndFeels, cxLookAndFeels, dxSkinsForm, Inifiles,
  System.ImageList, Vcl.ImgList, Vcl.Controls, cxImageList, cxGraphics,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, Data.FireDACJSONReflect,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.Dialogs, Vcl.Forms,
  FireDAC.Stan.StorageXML, FireDAC.Stan.StorageBin, cxLocalization,
  cxLabel, cxButtons, cxTextEdit, cxEdit, Winapi.Windows, VCL.Graphics;

type
  TfrmData = class(TDataModule)
    SkinController: TdxSkinController;
    LayoutLAFList: TdxLayoutLookAndFeelList;
    LayoutSkinLookAndFeel: TdxLayoutSkinLookAndFeel;
    ImageList_24x24: TcxImageList;
    FD_INVOICE: TFDMemTable;
    FD_INVOICEID: TIntegerField;
    FD_INVOICEDIRECTORY_NUMBER: TIntegerField;
    FD_INVOICEREFERENCE: TStringField;
    FD_INVOICEID_CUSTOMER: TIntegerField;
    FD_INVOICEID_BUILDING_SITE: TIntegerField;
    FD_INVOICELABEL: TStringField;
    FD_INVOICECREATION_DATE: TDateField;
    FD_INVOICEMODIFICATION_DATE: TDateField;
    FD_INVOICEID_CREATOR: TIntegerField;
    FD_INVOICEID_MODIFIER: TIntegerField;
    FD_INVOICEID_STATE: TIntegerField;
    FD_INVOICEID_CATEGORY: TIntegerField;
    FD_INVOICEID_TVA: TIntegerField;
    FD_INVOICETTC_AMOUNT: TFMTBCDField;
    FD_INVOICEHT_AMOUNT: TFMTBCDField;
    FD_INVOICEID_HOLDBACK: TIntegerField;
    FDStanStorageXMLLink: TFDStanStorageXMLLink;
    Localizer: TcxLocalizer;
    FD_CUSTOMER: TFDMemTable;
    FD_CUSTOMERID: TIntegerField;
    FD_CUSTOMERNAME: TStringField;
    FD_CUSTOMERPRIVATE: TIntegerField;
    FD_CUSTOMERCONDITION: TStringField;
    FD_CUSTOMERPHONE: TStringField;
    FD_CUSTOMERSMARTPHONE: TStringField;
    FD_CUSTOMEREMAIL: TStringField;
    FD_CUSTOMERADDRESS: TStringField;
    FD_CUSTOMERPOSTAL_CODE: TIntegerField;
    FD_CUSTOMERCITY: TStringField;
    FD_USER: TFDMemTable;
    FD_USERID: TIntegerField;
    FD_USERNAME: TStringField;
    FD_USERFIRSTNAME: TStringField;
    FD_USERSMARTPHONE: TStringField;
    FD_USERPHONE: TStringField;
    FD_USEREMAIL: TStringField;
    FD_USERID_JOB: TIntegerField;
    FD_USERFULLNAME: TStringField;
    FD_CUSTOMERCONTACT: TStringField;
    FD_CREATEUR: TFDMemTable;
    FD_CREATEURID: TIntegerField;
    FD_CREATEURFULLNAME: TStringField;
    FD_USERID_CREATOR: TIntegerField;
    FD_USERDATE_CREATION: TDateField;
    FD_USERPERSONAL_SMARTPHONE: TStringField;
    FD_USERPERSONAL_EMAIL: TStringField;
    FD_JOB: TFDMemTable;
    FD_JOBID: TIntegerField;
    FD_JOBLABEL: TStringField;
    FD_USERID_MODIFIER: TIntegerField;
    FD_USERDATE_MODIFICATION: TDateField;
    FD_CUSTOMERID_CREATOR: TIntegerField;
    FD_CUSTOMERDATE_CREATION: TDateField;
    FD_CUSTOMERID_MODIFIER: TIntegerField;
    FD_CUSTOMERDATE_MODIFICATION: TDateField;
    FD_JTB_INVOICE: TFDMemTable;
    FD_JTB_INVOICEID: TIntegerField;
    FD_JTB_INVOICEDIRECTORY_NUMBER: TIntegerField;
    FD_JTB_INVOICEREFERENCE: TStringField;
    FD_JTB_INVOICEID_CUSTOMER: TIntegerField;
    FD_JTB_INVOICENAME_CUSTOMER: TStringField;
    FD_JTB_INVOICEEMAIL_CUSTOMER: TStringField;
    FD_JTB_INVOICEADDRESS_CUSTOMER: TStringField;
    FD_JTB_INVOICECITY_CUSTOMER: TStringField;
    FD_JTB_INVOICEPOSTAL_CODE_CUSTOMER: TIntegerField;
    FD_JTB_INVOICELABEL: TStringField;
    FD_JTB_INVOICECREATION_DATE: TDateField;
    FD_JTB_INVOICEID_CREATOR: TIntegerField;
    FD_JTB_INVOICENOM_CREATOR: TStringField;
  private
    { D�clarations priv�es }
    procedure Login();
  public
    { D�clarations publiques }
    id_user : integer;
    name_user, firstname_user, email_user : string;

    procedure FDMemGetTables(TablesList: string);
    procedure FDMemUpdate(TablesList: string);
    function ExecProc(ProcName, ParamsList, ValuesList, ResultsList: string): String;
    function SetParameters(Table: string): string;
    procedure Initialisation();

    function GetCharSize(Canvas: TCanvas): TPoint;
    function InputTextEdit(const ACaption, APrompt: string): string;

    function InvoiceAdd(ValuesList: string) : integer;
    function InvoicePaste(ValuesList : string) : integer;

    function UserAdd(ValuesList: string) : integer;
    function JobAdd() : integer;
    function JobDel(ValuesList : string) : Integer;

    function CustomerAdd(ValuesList: string) : Integer;
  end;

var
  frmData: TfrmData;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

Uses
  EMPDevisClientModuleUnit;

{$R *.dfm}

procedure TfrmData.Initialisation();
begin
  Localizer.Locale := 1036;
  FDMemGetTables('FD_USER');

  Login();
end;

procedure TfrmData.Login();
var
  FileIni : TInifile;
  Email : String;
begin
  if FileExists(extractFilePath(application.exename) + 'Login.ini') then
  begin
    FileIni := Tinifile.Create(extractFilePath(application.exename) + 'Login.ini');

    id_user := FileIni.ReadInteger('Login','ID', 0);
    name_user := FileIni.ReadString('Login','Nom', '');
    firstname_user := FileIni.ReadString('Login','Pr�nom', '');
    email_user := FileIni.ReadString('Login','Email', '');
  end
  else
  begin
    FileIni := Tinifile.Create(extractFilePath(application.exename) + 'Login.ini');

    Email := InputTextEdit('Identification', 'Saisissez votre adresse email professionnel pour vous identifier.');

    while (not FD_USER.Locate('EMAIL', Email, [loCaseInsensitive])) and (Email <> 'Annuler') do
    begin
      ShowMessage('Email non r�pertori�.');

      Email := InputTextEdit('Identification', 'Saisissez votre adresse email professionnel pour vous identifier.');
    end;

    if Email = 'Annuler' then
      Application.MainForm.Close
    else
    begin
      FileIni.WriteInteger('Login', 'ID', FD_USERID.AsInteger);
      FileIni.WriteString('Login', 'Nom', FD_USERNAME.AsString);
      FileIni.WriteString('Login', 'Pr�nom', FD_USERFIRSTNAME.AsString);
      FileIni.WriteString('Login', 'Email', FD_USEREMAIL.AsString);
    end;
  end;
end;

function TfrmData.GetCharSize(Canvas: TCanvas): TPoint;
var
  I: Integer;
  Buffer: array[0..51] of Char;
begin
  for I := 0 to 25 do Buffer[I] := Chr(I + Ord('A'));
  for I := 0 to 25 do Buffer[I + 26] := Chr(I + Ord('a'));
  GetTextExtentPoint(Canvas.Handle, Buffer, 52, TSize(Result));
  Result.X := Result.X div 52;
end;

function TfrmData.InputTextEdit(const ACaption, APrompt: string): string;
var
  Form: TForm;
  Prompt: TcxLabel;
  TextEdit : TcxTextEdit;
  DialogUnits: TPoint;
  ButtonTop, ButtonWidth, ButtonHeight: Integer;
begin
  Result := '';
  Form := TForm.Create(Application);
  with Form do
    try
      Canvas.Font := Font;
      DialogUnits := GetCharSize(Canvas);
      BorderStyle := bsDialog;
      Caption := ACaption;
      ClientWidth := MulDiv(180, DialogUnits.X, 4);
      Position := poMainFormCenter;
      Prompt := TcxLabel.Create(Form);
      with Prompt do
      begin
        Parent := Form;
        Caption := APrompt;
        Left := MulDiv(5, DialogUnits.X, 4);
        Top := MulDiv(5, DialogUnits.Y, 8);
        Width := MulDiv(164, DialogUnits.X, 4);
        Constraints.MaxWidth := MulDiv(164, DialogUnits.X, 4);
        Transparent := true;
        Properties.WordWrap := True;
      end;
      TextEdit := TcxTextEdit.Create(Form);
      with TextEdit do
      begin
        Parent := Form;
        Properties.Alignment.Horz := TAlignment.taCenter;
        Left := Prompt.Left;
        Top := Prompt.Top + Prompt.Height + 5;
        Width := MulDiv(164, DialogUnits.X, 4);
      end;
      ButtonTop := TextEdit.Top + TextEdit.Height + 15;
      ButtonWidth := MulDiv(50, DialogUnits.X, 4);
      ButtonHeight := MulDiv(14, DialogUnits.Y, 8);
      with TcxButton.Create(Form) do
      begin
        Parent := Form;
        Caption := 'OK';
        ModalResult := mrOk;
        default := True;
        SetBounds(MulDiv(38, DialogUnits.X, 4), ButtonTop, ButtonWidth, ButtonHeight);
      end;
      with TcxButton.Create(Form) do
      begin
        Parent := Form;
        Caption := 'Annuler';
        ModalResult := mrCancel;
        Cancel := True;
        SetBounds(MulDiv(92, DialogUnits.X, 4), TextEdit.Top + TextEdit.Height + 15, ButtonWidth, ButtonHeight);
        Form.ClientHeight := Top + Height + 13;
      end;
      if ShowModal = mrOk then
        Result := TextEdit.Text
      else
        Result := 'Annuler';
    finally
      Form.Free;
    end;
end;

function TfrmData.UserAdd(ValuesList: string) : Integer;
var
  r_id_user : string;
begin
  r_id_user := ExecProc('SP_USER_ADD', 'P_ID_CREATEUR', ValuesList, 'r_id_user');
  result := StrToInt(r_id_user);
end;

function TfrmData.InvoiceAdd(ValuesList : string) : Integer;
var
  r_id_invoice : string;
begin
  r_id_invoice := ExecProc('SP_INVOICE_ADD', 'P_ID_USER', ValuesList, 'r_id_invoice');
  result := StrToInt(r_id_invoice);
end;

function TfrmData.InvoicePaste(ValuesList : string) : integer;
var
  r_id_invoice : string;
begin
  r_id_invoice := ExecProc('SP_INVOICE_PASTE', 'P_ID_INVOICE;P_ID_USER', ValuesList, 'r_id_invoice');
  result := StrToInt(r_id_invoice);
end;

function TFrmData.ExecProc(ProcName, ParamsList, ValuesList, ResultsList: string): String;
begin
  ClientModule := TClientModule.Create(Application);
  try
    Result := ClientModule.ServerMethodsClient.ExecProc(ProcName, ParamsList, ValuesList, ResultsList);
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  ClientModule.Free;
end;

procedure TFrmData.FDMemUpdate(TablesList: string);
var
  LDeltaList: TFDJSONDeltas;
  ListTable: TStringList;
  LocalTable: TFDMemTable;
  i: Integer;
begin
{$IFDEF ANDROID}
  Application.MainForm.Cursor := crHourGlass;
{$ENDIF ANDROID}
{$IFDEF MSWINDOWS}
  screen.Cursor := crHourGlass;
{$ENDIF MSWINDOWS}
  LDeltaList := TFDJSONDeltas.Create;
  ListTable := TStringList.Create;
  try
    ListTable.Delimiter := ';';
    ListTable.DelimitedText := TablesList;
    for i := 0 to ListTable.Count - 1 do
    begin
      LocalTable := FindComponent(ListTable[i]) as TFDMemTable;
      TFDJSONDeltasWriter.ListAdd(LDeltaList, LocalTable.Name, LocalTable);
    end;
    ClientModule := TClientModule.Create(Application);
    ClientModule.ServerMethodsClient.UpdTables(ListTable.Text, LDeltaList);
    ClientModule.Free;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  ListTable.Free;
{$IFDEF ANDROID}
  Application.MainForm.Cursor := crDefault;
{$ENDIF ANDROID}
{$IFDEF MSWINDOWS}
  screen.Cursor := crDefault;
{$ENDIF MSWINDOWS}
end;

//Permet de gerer plusieurs param�tres d'une m�me requ�te
procedure TfrmData.FDMemGetTables(TablesList: string);
var
  LDataSetList: TFDJSONDataSets;
  ListTables, ListParams: TStringList;
  LocalTable: TFDMemTable;
  i: Integer;
begin
{$IFDEF ANDROID}
  Application.MainForm.Cursor := crHourGlass;
{$ENDIF ANDROID}
{$IFDEF MSWINDOWS}
  screen.Cursor := crHourGlass;
{$ENDIF MSWINDOWS}

  ListTables := TStringList.Create;
  ListTables.Delimiter := ';';
  ListTables.DelimitedText := TablesList;
  ListParams := TStringList.Create;
  ListParams.Delimiter := ';';

  for i := 0 to ListTables.Count - 1 do
  begin
    ListParams.Add(SetParameters(ListTables[i]));
  end;

  ClientModule := TClientModule.Create(Application);
  try
    Application.MainForm.Cursor := crHourGlass;
    for i := 0 to ListTables.Count - 1 do
    begin
      LDataSetList := ClientModule.ServerMethodsClient.GetTables(ListTables[i], ListParams[i]);
      LocalTable := FindComponent(ListTables[i]) as TFDMemTable;
      LocalTable.Close;
      LocalTable.AppendData(TFDJSONDataSetsReader.GetListValueByName(LDataSetList, ListTables[i]));
    end;
  except
    on E: Exception do
      ShowMessage(E.Message);
  end;
  ListTables.Free;
  ClientModule.Free;

{$IFDEF ANDROID}
  Application.MainForm.Cursor := crDefault;
{$ENDIF ANDROID}
{$IFDEF MSWINDOWS}
  screen.Cursor := crDefault;
{$ENDIF MSWINDOWS}
end;

// Les param�tres vouluent � la requ�te
function TfrmData.SetParameters(Table: string): string;
begin
  Result := '*';
end;

//// ***************** ////
//// ***** Users ***** ////
//// ***************** ////
{$region Users}
function TfrmData.JobAdd() : integer;
var
  r_id_job : string;
begin
  r_id_job := ExecProc('SP_JOB_ADD', '', '', 'r_id_job');
  result := StrToInt(r_id_job);
end;

function TfrmData.JobDel(ValuesList : string) : Integer;
var
  r_id_job : string;
begin
  r_id_job := ExecProc('SP_JOB_DEL', 'p_id_job', ValuesList, 'r_id_job');
  result := StrToInt(r_id_job);
end;
{$endregion}

//// ******************** ////
//// ***** Customer ***** ////
//// ******************** ////
{$region Customer}
function TfrmData.CustomerAdd(ValuesList: string) : Integer;
var
  r_id_customer : string;
begin
  r_id_customer := ExecProc('SP_CUSTOMER_ADD', 'P_ID_USER', ValuesList, 'r_id_customer');
  result := StrToInt(r_id_customer);
end;
{$endregion}

end.
