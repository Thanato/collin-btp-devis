unit EMPDevisCustomer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, EMPDevisFrame, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, BDLSkin,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxLayoutContainer, cxClasses, dxLayoutControl, dxLayoutControlAdapters,
  Vcl.Menus, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, dxDateRanges, dxScrollbarAnnotations, Data.DB, cxDBData,
  cxCheckBox, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxGridCustomView, cxGrid, Vcl.StdCtrls, cxButtons, cxContainer,
  cxMaskEdit, cxDropDownEdit, cxDBEdit, cxTextEdit, dxLayoutcxEditAdapters,
  cxCalendar, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, System.Actions,
  Vcl.ActnList;

type
  TfrmCustomer = class(TfrmFrame)
    LayoutControl: TdxLayoutControl;
    LayoutControlGroup_Root: TdxLayoutGroup;
    LayoutGroup_Navigation: TdxLayoutGroup;
    LayoutGroup_Body: TdxLayoutGroup;
    LayoutItem_btn_Refresh: TdxLayoutItem;
    LayoutSeparatorItem: TdxLayoutSeparatorItem;
    LayoutItem_btn_Add: TdxLayoutItem;
    LayoutItem_btn_Save: TdxLayoutItem;
    LayoutGroup_grd_User: TdxLayoutGroup;
    LayoutGroup_Informations: TdxLayoutGroup;
    LayoutGroup_PDF: TdxLayoutGroup;
    LayoutSeparatorItem1: TdxLayoutSeparatorItem;
    LayoutItem_grd_User: TdxLayoutItem;
    LayoutGroup_PDF_Navigation: TdxLayoutGroup;
    LayoutItem_btn_Export: TdxLayoutItem;
    LayoutItem_grd_PDF: TdxLayoutItem;
    LayoutGroup_InformationsClient: TdxLayoutGroup;
    LayoutItem_txtedt_Name: TdxLayoutItem;
    LayoutItem_chkbox_Private: TdxLayoutItem;
    LayoutGroup_InformationClient_Line1: TdxLayoutGroup;
    LayoutItem_cmb_ConditionReglement: TdxLayoutItem;
    LayoutGroup_InformationsClient_Line2: TdxLayoutGroup;
    LayoutItem_txtedt_Smatphone: TdxLayoutItem;
    LayoutItem_txtedt_Phone: TdxLayoutItem;
    LayoutItem_txtedt_Address: TdxLayoutItem;
    LayoutItem_txtedt_Ville: TdxLayoutItem;
    LayoutItem_txtedt_CP: TdxLayoutItem;
    LayoutGroup_InformationsClient_Line3: TdxLayoutGroup;
    LayoutItem_txtedt_NomContact: TdxLayoutItem;
    LayoutItem_txtedt_Email: TdxLayoutItem;
    LayoutGroup_InformationsClient_Line4: TdxLayoutGroup;
    LayoutGroup_Creator: TdxLayoutGroup;
    LayoutItem_lkpcmb_Creator: TdxLayoutItem;
    LayoutItem_datedt_Creation: TdxLayoutItem;
    btn_Refresh: TcxButton;
    btn_Add: TcxButton;
    btn_Save: TcxButton;
    grd_CustomerDBTableView: TcxGridDBTableView;
    grd_CustomerLevel: TcxGridLevel;
    grd_Customer: TcxGrid;
    DS_CUSTOMER: TDataSource;
    grd_CustomerDBTableViewID: TcxGridDBColumn;
    grd_CustomerDBTableViewNAME: TcxGridDBColumn;
    grd_CustomerDBTableViewPRIVATE: TcxGridDBColumn;
    grd_CustomerDBTableViewCONDITION: TcxGridDBColumn;
    grd_CustomerDBTableViewPHONE: TcxGridDBColumn;
    grd_CustomerDBTableViewSMARTPHONE: TcxGridDBColumn;
    grd_CustomerDBTableViewEMAIL: TcxGridDBColumn;
    grd_CustomerDBTableViewADDRESS: TcxGridDBColumn;
    grd_CustomerDBTableViewPOSTAL_CODE: TcxGridDBColumn;
    grd_CustomerDBTableViewCONTACT: TcxGridDBColumn;
    grd_CustomerDBTableViewCITY: TcxGridDBColumn;
    grd_CustomerDBTableViewDATE_MODIFICATION: TcxGridDBColumn;
    grd_CustomerDBTableViewID_MODIFIER: TcxGridDBColumn;
    grd_CustomerDBTableViewDATE_CREATION: TcxGridDBColumn;
    grd_CustomerDBTableViewID_CREATOR: TcxGridDBColumn;
    grd_PDFDBTableView: TcxGridDBTableView;
    grd_PDFLevel: TcxGridLevel;
    grd_PDF: TcxGrid;
    btn_PDF: TcxButton;
    LayoutSplitterItem1: TdxLayoutSplitterItem;
    LayoutSplitterItem2: TdxLayoutSplitterItem;
    LayoutGroup_Informations2: TdxLayoutGroup;
    txtedt_Name: TcxDBTextEdit;
    chkbox_Private: TcxDBCheckBox;
    cmb_ConditionReglement: TcxDBComboBox;
    txtedt_Adresse: TcxDBTextEdit;
    txtedt_Ville: TcxDBTextEdit;
    txtedt_CodePostal: TcxDBTextEdit;
    txtedt_NomContact: TcxDBTextEdit;
    txtedt_Email: TcxDBTextEdit;
    txtedt_Phone: TcxDBTextEdit;
    txtedt_Smatphone: TcxDBTextEdit;
    LayoutItem_lkpcmb_Modifier: TdxLayoutItem;
    LayoutItem_datedt_DateModification: TdxLayoutItem;
    lkpcmb_Creator: TcxDBLookupComboBox;
    lkpcmb_Modifier: TcxDBLookupComboBox;
    datedt_DateCreation: TcxDBDateEdit;
    datedt_DateModification: TcxDBDateEdit;
    DS_CREATEUR: TDataSource;
    ActionList_Navigation: TActionList;
    ActionRefresh: TAction;
    ActionAdd: TAction;
    ActionSave: TAction;
    procedure ActionRefreshExecute(Sender: TObject);
    procedure ActionAddExecute(Sender: TObject);
    procedure ActionSaveExecute(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    constructor Create(AOwner: TComponent); override;
  end;

var
  frmCustomer: TfrmCustomer;

implementation

{$R *.dfm}

Uses
  EMPDevisData;

procedure TfrmCustomer.ActionAddExecute(Sender: TObject);
var
  id_customer : integer;
begin
  inherited;
  try
    id_customer := frmData.CustomerAdd(IntToStr(frmData.id_user));
    frmData.FDMemGetTables('FD_CUSTOMER');
    frmData.FD_CUSTOMER.Locate('ID', id_customer, [loCaseInsensitive]);

    ShowMessage('Nouveau client cr��.');
  except on E: Exception do
    ShowMessage('Une erreur est survenue lors de la cr�ation d''un client.');
  end;
end;

procedure TfrmCustomer.ActionRefreshExecute(Sender: TObject);
begin
  inherited;
  frmData.FDMemGetTables('FD_CUSTOMER');
end;

procedure TfrmCustomer.ActionSaveExecute(Sender: TObject);
begin
  inherited;
  if (frmData.FD_CUSTOMER.State = dsEdit) or (frmData.FD_CUSTOMER.State = dsInsert) then
  begin
    try
      frmData.FD_CUSTOMER.Edit;
      frmData.FD_CUSTOMERID_MODIFIER.AsInteger := frmData.id_user;
      frmData.FD_CUSTOMERDATE_MODIFICATION.AsDateTime := date();
      frmData.FD_CUSTOMER.Post;
      frmData.FDMemUpdate('FD_CUSTOMER');

      ShowMessage('Le client ' + txtedt_Name.Text + ' a �t� sauvegard�.');
    except on E: Exception do
      Showmessage('Une erreur est survenue lors de l''enregistrement du client ' +
                    txtedt_Name.Text + '.');
    end;

  end
  else
  begin
    ShowMessage('Aucunes modifications apport�es.');
  end;
end;

Constructor TfrmCustomer.Create(AOwner: TComponent);
begin
  inherited;
  frmData.FDMemGetTables('FD_CUSTOMER');
end;

initialization
RegisterFrame(IDCustomer, TfrmCustomer);

end.
