unit EMPDevisUserConfiguration;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, BDLSkin,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  cxClasses, dxLayoutContainer, dxLayoutControl, cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, Data.DB, cxDBData, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;

type
  TfrmUserConfiguration = class(TForm)
    LayoutControlGroup_Root: TdxLayoutGroup;
    LayoutControl: TdxLayoutControl;
    LayoutGroup_Configuration: TdxLayoutGroup;
    LayoutItem_grd_Fonction: TdxLayoutItem;
    grd_FonctionLevel: TcxGridLevel;
    grd_Fonction: TcxGrid;
    grd_FonctionDBTableView: TcxGridDBTableView;
    DS_JOB: TDataSource;
    grd_FonctionDBTableViewID: TcxGridDBColumn;
    grd_FonctionDBTableViewLABEL: TcxGridDBColumn;
    procedure grd_FonctionDBTableViewNavigatorButtonsButtonClick(
      Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  frmUserConfiguration: TfrmUserConfiguration;

  procedure CreateFormUserConfiguration();

implementation

{$R *.dfm}

Uses
  EMPDevisData;

procedure CreateFormUserConfiguration();
begin
  frmUserConfiguration := TfrmUserConfiguration.Create(Application);
  try

    frmUserConfiguration.ShowModal();
  finally
    FreeAndNil(frmUserConfiguration);
  end;
end;

procedure TfrmUserConfiguration.FormCreate(Sender: TObject);
begin
  frmdata.FDMemGetTables('FD_JOB');
end;

procedure TfrmUserConfiguration.grd_FonctionDBTableViewNavigatorButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
var
  Index, n_line: Integer;
begin
  inherited;
  // On calcul l'index du button s'il est custom
  if AButtonIndex >= 16 then
  begin
    index := (TcxGridTableViewNavigatorButtons(Sender).ButtonCount - AButtonIndex);
    index := TcxGridTableViewNavigatorButtons(Sender).CustomButtons.Count - index;

    if TcxGridTableViewNavigatorButtons(Sender).CustomButtons.Items[index].Hint = 'Add' then
    begin
      n_line := frmData.JobAdd();
      frmData.FDMemGetTables('FD_JOB');
      frmData.FD_JOB.Locate('ID', n_line, [loCaseInsensitive]);
    end
    else if TcxGridTableViewNavigatorButtons(Sender).CustomButtons.Items[index].Hint = 'Delete' then
    begin
      if MessageDLG('Voulez-vous supprimer cette ligne ?', mtWarning, mbYesNo, 0) = mrYes then
      begin
        frmData.JobDel(frmData.FD_JOBID.AsString);
        frmData.FDMemGetTables('FD_JOB');
      end;
    end
    else if TcxGridTableViewNavigatorButtons(Sender).CustomButtons.Items[index].Hint = 'Update' then
    begin
      if frmData.FD_JOB.State = dsEdit then
      begin
        frmData.FD_JOB.Post;
        frmData.FDMemUpdate('FD_JOB');
        frmData.FDMemGetTables('FD_JOB');
        ShowMessage('Fonction sauvegardés.');
      end;
    end
    else if TcxGridTableViewNavigatorButtons(Sender).CustomButtons.Items[index].Hint = 'Cancel' then
    begin
      if MessageDLG('Voulez-vous annuler toutes les modfications ?', mtWarning, mbYesNo, 0) = mrYes then
      begin
        frmData.FDMemGetTables('FD_JOB');
      end;
    end
    else if TcxGridTableViewNavigatorButtons(Sender).CustomButtons.Items[index].Hint = 'Cancel' then
    begin
      if MessageDLG('Voulez-vous annuler toutes les modfications ?', mtWarning, mbYesNo, 0) = mrYes then
      begin
        frmData.FDMemGetTables('FD_JOB');
      end;
    end;
  end;
end;

end.
