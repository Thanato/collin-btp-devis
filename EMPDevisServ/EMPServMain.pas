unit EMPServMain;

interface

uses
  Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, Web.HTTPApp, Vcl.ExtCtrls,
  Vcl.Menus;

type
  TfrmServeur = class(TForm)
    ButtonStart: TButton;
    ButtonStop: TButton;
    EditPort: TEdit;
    Label1: TLabel;
    ApplicationEvents1: TApplicationEvents;
    ButtonOpenBrowser: TButton;
    TrayIcon: TTrayIcon;
    PopupMenu: TPopupMenu;
    itm_Visible: TMenuItem;
    itm_Close: TMenuItem;
    Image1: TImage;
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure ButtonOpenBrowserClick(Sender: TObject);
    procedure ApplicationEvents1Minimize(Sender: TObject);
    procedure TrayIconDblClick(Sender: TObject);
    procedure itm_CloseClick(Sender: TObject);
    procedure itm_VisibleClick(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;
    procedure StartServer;
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  frmServeur: TfrmServeur;

implementation

{$R *.dfm}

uses
  WinApi.Windows, Winapi.ShellApi, Datasnap.DSSession;

procedure TfrmServeur.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  ButtonStart.Enabled := not FServer.Active;
  ButtonStop.Enabled := FServer.Active;
  EditPort.Enabled := not FServer.Active;
end;

procedure TfrmServeur.ApplicationEvents1Minimize(Sender: TObject);
var
  k: integer;
begin
  application.Minimize;
  for k := 0 to Screen.FormCount - 1 do
  begin
    Screen.Forms[k].Hide;
  end;
end;

procedure TfrmServeur.ButtonOpenBrowserClick(Sender: TObject);
var
  LURL: string;
begin
  StartServer;
  LURL := Format('http://localhost:%s', [EditPort.Text]);
  ShellExecute(0,
        nil,
        PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

procedure TfrmServeur.ButtonStartClick(Sender: TObject);
begin
  StartServer;
end;

procedure TerminateThreads;
begin
  if TDSSessionManager.Instance <> nil then
    TDSSessionManager.Instance.TerminateAllSessions;
end;

procedure TfrmServeur.ButtonStopClick(Sender: TObject);
begin
  TerminateThreads;
  FServer.Active := False;
  FServer.Bindings.Clear;
end;

procedure TfrmServeur.FormCreate(Sender: TObject);
begin
  FServer := TIdHTTPWebBrokerBridge.Create(Self);
  StartServer();
end;

procedure TfrmServeur.itm_CloseClick(Sender: TObject);
begin
  close;
end;

procedure TfrmServeur.itm_VisibleClick(Sender: TObject);
var
  k: integer;
begin
  for k := Screen.FormCount - 1 downto 0 do
  begin
    Screen.Forms[k].Show;
  end;
  application.Restore;
  application.BringToFront;
end;

procedure TfrmServeur.StartServer;
begin
  if not FServer.Active then
  begin
    FServer.Bindings.Clear;
    FServer.DefaultPort := StrToInt(EditPort.Text);
    FServer.Active := True;
  end;
end;

procedure TfrmServeur.TrayIconDblClick(Sender: TObject);
var
  k: integer;
begin
  for k := Screen.FormCount - 1 downto 0 do
  begin
    Screen.Forms[k].Show;
  end;
  application.Restore;
  application.BringToFront;
end;

end.
