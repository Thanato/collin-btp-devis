object ServerMethods: TServerMethods
  OldCreateOrder = False
  Height = 344
  Width = 575
  object FDPhysFBDriverLink: TFDPhysFBDriverLink
    Left = 45
    Top = 3
  end
  object FD_Connection: TFDConnection
    Params.Strings = (
      'Database=C:\DATA\EMP.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=ISO8859_1'
      'Protocol=TCPIP'
      'Server=localhost'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 45
    Top = 31
  end
  object FDGUIxWaitCursor: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 45
    Top = 59
  end
  object FDStanStorageBinLink: TFDStanStorageBinLink
    Left = 45
    Top = 87
  end
  object FDStoredProc: TFDStoredProc
    Connection = FD_Connection
    StoredProcName = 'VALIDATION_NEW_AGENCE'
    Left = 45
    Top = 115
    ParamData = <
      item
        Position = 1
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'NEW_ID'
        DataType = ftInteger
        ParamType = ptOutput
      end>
  end
  object FDUpdateORGA: TFDUpdateSQL
    Connection = FD_Connection
    InsertSQL.Strings = (
      'INSERT INTO P_USER'
      '(ID, ID_SERVICE, ID_FONCTION, ID_SITE, ORG_PARENT, '
      '  ORG_ORDER)'
      
        'VALUES (:NEW_ID, :NEW_ID_SERVICE, :NEW_ID_FONCTION, :NEW_ID_SITE' +
        ', :NEW_ORG_PARENT, '
      '  :NEW_ORG_ORDER)')
    ModifySQL.Strings = (
      'UPDATE P_USER'
      
        'SET ID = :NEW_ID, ID_SERVICE = :NEW_ID_SERVICE, ID_FONCTION = :N' +
        'EW_ID_FONCTION, '
      
        '  ID_SITE = :NEW_ID_SITE, ORG_PARENT = :NEW_ORG_PARENT, ORG_ORDE' +
        'R = :NEW_ORG_ORDER'
      'WHERE ID = :OLD_ID')
    DeleteSQL.Strings = (
      'DELETE FROM P_USER'
      'WHERE ID = :OLD_ID')
    FetchRowSQL.Strings = (
      
        'SELECT ID, CODEP, NOM, PRENOM, ACTIF, EN_POSTE, ID_SOCIETE, ID_A' +
        'GENCE, '
      '  ID_SERVICE, ID_FONCTION, ID_SITE, TELEPHONE, POSTE, PORTABLE, '
      
        '  EMAIL, NUM_EASY, DATE_SORTIE, PASS_GESTCOM, MDP, MDP_PHOTOCOPI' +
        'EUR, '
      '  MDP_GOOGLE, MDP_COPIEUR, SOFT_ACCESS, F_RECHERCHE, F_CLIENT, '
      '  F_DESSIN, F_MODIF, F_VOLET, F_METRE, F_MAP, F_CARRELAGE, '
      '  F_SUIVITBET, F_AVENANT, F_SATB, F_BDCSATB, F_CREDIT_IMPOT, '
      
        '  F_FINANCE, F_FACTCLI, F_TRAVAUX, F_MEMO, F_GED, F_SAV, F_JURID' +
        'IQUE, '
      '  F_VT, F_PRIME_TRX, F_ASSURANCE, F_ETUDE_THERM, F_ANNULATION, '
      '  F_EASY, F_PARRAINAGE, NTIERS, PERMIS_NUM, PERMIS_DATEOBT, '
      '  PERMIS_LIEU, ORG_PARENT, ORG_ORDER, PHOTO, DATE_CONTRAT, '
      '  TYPE_CONTRAT, TYPE_POSTE'
      'FROM P_USER'
      'WHERE ID = :ID')
    Left = 45
    Top = 143
  end
  object FD_INVOICE: TFDQuery
    Connection = FD_Connection
    SQL.Strings = (
      'select'
      '  id,'
      '  directory_number,'
      '  reference,'
      '  id_customer,'
      '  id_building_site,'
      '  label,'
      '  creation_date,'
      '  modification_date,'
      '  id_creator,'
      '  id_modifier,'
      '  id_state,'
      '  id_category,'
      '  id_tva,'
      '  ttc_amount,'
      '  ht_amount,'
      '  id_holdback,'
      '  id_invoice_associate'
      'from'
      '  invoice'
      'order by'
      '  directory_number desc')
    Left = 176
    Top = 24
    object FD_INVOICEID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FD_INVOICEDIRECTORY_NUMBER: TIntegerField
      FieldName = 'DIRECTORY_NUMBER'
      Origin = 'DIRECTORY_NUMBER'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FD_INVOICEREFERENCE: TStringField
      FieldName = 'REFERENCE'
      Origin = 'REFERENCE'
      Size = 12
    end
    object FD_INVOICEID_CUSTOMER: TIntegerField
      FieldName = 'ID_CUSTOMER'
      Origin = 'ID_CUSTOMER'
    end
    object FD_INVOICEID_BUILDING_SITE: TIntegerField
      FieldName = 'ID_BUILDING_SITE'
      Origin = 'ID_BUILDING_SITE'
    end
    object FD_INVOICELABEL: TStringField
      FieldName = 'LABEL'
      Origin = 'LABEL'
      Size = 50
    end
    object FD_INVOICECREATION_DATE: TDateField
      FieldName = 'CREATION_DATE'
      Origin = 'CREATION_DATE'
    end
    object FD_INVOICEMODIFICATION_DATE: TDateField
      FieldName = 'MODIFICATION_DATE'
      Origin = 'MODIFICATION_DATE'
    end
    object FD_INVOICEID_CREATOR: TIntegerField
      FieldName = 'ID_CREATOR'
      Origin = 'ID_CREATOR'
    end
    object FD_INVOICEID_MODIFIER: TIntegerField
      FieldName = 'ID_MODIFIER'
      Origin = 'ID_MODIFIER'
    end
    object FD_INVOICEID_STATE: TIntegerField
      FieldName = 'ID_STATE'
      Origin = 'ID_STATE'
    end
    object FD_INVOICEID_CATEGORY: TIntegerField
      FieldName = 'ID_CATEGORY'
      Origin = 'ID_CATEGORY'
    end
    object FD_INVOICEID_TVA: TIntegerField
      FieldName = 'ID_TVA'
      Origin = 'ID_TVA'
    end
    object FD_INVOICETTC_AMOUNT: TFMTBCDField
      FieldName = 'TTC_AMOUNT'
      Origin = 'TTC_AMOUNT'
      Precision = 18
      Size = 2
    end
    object FD_INVOICEHT_AMOUNT: TFMTBCDField
      FieldName = 'HT_AMOUNT'
      Origin = 'HT_AMOUNT'
      Precision = 18
      Size = 2
    end
    object FD_INVOICEID_HOLDBACK: TIntegerField
      FieldName = 'ID_HOLDBACK'
      Origin = 'ID_HOLDBACK'
    end
    object FD_INVOICEID_INVOICE_ASSOCIATE: TIntegerField
      FieldName = 'ID_INVOICE_ASSOCIATE'
      Origin = 'ID_INVOICE_ASSOCIATE'
    end
  end
  object FD_CUSTOMER: TFDQuery
    Connection = FD_Connection
    SQL.Strings = (
      'select'
      '  id,'
      '  name,'
      '  private,'
      '  condition,'
      '  phone,'
      '  smartphone,'
      '  email,'
      '  address,'
      '  postal_code,'
      '  city,'
      '  contact,'
      '  id_creator,'
      '  date_creation,'
      '  id_modifier,'
      '  date_modification'
      'from'
      '  customer')
    Left = 176
    Top = 88
    object FD_CUSTOMERID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FD_CUSTOMERNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 50
    end
    object FD_CUSTOMERPRIVATE: TIntegerField
      FieldName = 'PRIVATE'
      Origin = 'PRIVATE'
    end
    object FD_CUSTOMERCONDITION: TStringField
      FieldName = 'CONDITION'
      Origin = 'CONDITION'
      Size = 250
    end
    object FD_CUSTOMERPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 14
    end
    object FD_CUSTOMERSMARTPHONE: TStringField
      FieldName = 'SMARTPHONE'
      Origin = 'SMARTPHONE'
      Size = 14
    end
    object FD_CUSTOMEREMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 50
    end
    object FD_CUSTOMERADDRESS: TStringField
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 50
    end
    object FD_CUSTOMERPOSTAL_CODE: TIntegerField
      FieldName = 'POSTAL_CODE'
      Origin = 'POSTAL_CODE'
    end
    object FD_CUSTOMERCITY: TStringField
      FieldName = 'CITY'
      Origin = 'CITY'
      Size = 50
    end
    object FD_CUSTOMERCONTACT: TStringField
      FieldName = 'CONTACT'
      Origin = 'CONTACT'
      Size = 75
    end
    object FD_CUSTOMERID_CREATOR: TIntegerField
      FieldName = 'ID_CREATOR'
      Origin = 'ID_CREATOR'
    end
    object FD_CUSTOMERDATE_CREATION: TDateField
      FieldName = 'DATE_CREATION'
      Origin = 'DATE_CREATION'
    end
    object FD_CUSTOMERID_MODIFIER: TIntegerField
      FieldName = 'ID_MODIFIER'
      Origin = 'ID_MODIFIER'
    end
    object FD_CUSTOMERDATE_MODIFICATION: TDateField
      FieldName = 'DATE_MODIFICATION'
      Origin = 'DATE_MODIFICATION'
    end
  end
  object FD_USER: TFDQuery
    Connection = FD_Connection
    SQL.Strings = (
      'select'
      '  id,'
      '  name,'
      '  firstname,'
      '  smartphone,'
      '  phone,'
      '  email,'
      '  id_job,'
      '  firstname || '#39' '#39' || name as fullname,'
      '  id_creator,'
      '  date_creation,'
      '  personal_smartphone,'
      '  personal_email,'
      '  id_modifier,'
      '  date_modification'
      'from'
      '  "USER"')
    Left = 176
    Top = 144
    object FD_USERID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FD_USERNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Size = 50
    end
    object FD_USERFIRSTNAME: TStringField
      FieldName = 'FIRSTNAME'
      Origin = '"FIRSTNAME"'
      Size = 50
    end
    object FD_USERSMARTPHONE: TStringField
      FieldName = 'SMARTPHONE'
      Origin = 'SMARTPHONE'
      Size = 14
    end
    object FD_USERPHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 14
    end
    object FD_USEREMAIL: TStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      Size = 50
    end
    object FD_USERID_JOB: TIntegerField
      FieldName = 'ID_JOB'
      Origin = 'ID_JOB'
    end
    object FD_USERFULLNAME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 101
    end
    object FD_USERID_CREATOR: TIntegerField
      FieldName = 'ID_CREATOR'
      Origin = 'ID_CREATOR'
    end
    object FD_USERDATE_CREATION: TDateField
      FieldName = 'DATE_CREATION'
      Origin = 'DATE_CREATION'
    end
    object FD_USERPERSONAL_SMARTPHONE: TStringField
      FieldName = 'PERSONAL_SMARTPHONE'
      Origin = 'PERSONAL_SMARTPHONE'
      Size = 15
    end
    object FD_USERPERSONAL_EMAIL: TStringField
      FieldName = 'PERSONAL_EMAIL'
      Origin = 'PERSONAL_EMAIL'
      Size = 50
    end
    object FD_USERID_MODIFIER: TIntegerField
      FieldName = 'ID_MODIFIER'
      Origin = 'ID_MODIFIER'
    end
    object FD_USERDATE_MODIFICATION: TDateField
      FieldName = 'DATE_MODIFICATION'
      Origin = 'DATE_MODIFICATION'
    end
  end
  object FD_CREATEUR: TFDQuery
    Connection = FD_Connection
    SQL.Strings = (
      'select'
      '  ID,'
      '  firstname || '#39' '#39' || name as fullname'
      'from'
      '  "USER"')
    Left = 280
    Top = 144
    object FD_CREATEURID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FD_CREATEURFULLNAME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'FULLNAME'
      Origin = 'FULLNAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 101
    end
  end
  object FD_JOB: TFDQuery
    Connection = FD_Connection
    SQL.Strings = (
      'select'
      '  id,'
      '  label'
      'from'
      '  job')
    Left = 288
    Top = 96
    object FD_JOBID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FD_JOBLABEL: TStringField
      FieldName = 'LABEL'
      Origin = 'LABEL'
      Size = 50
    end
  end
  object FD_JTB_INVOICE: TFDQuery
    Connection = FD_Connection
    SQL.Strings = (
      'select'
      '  id,'
      '  directory_number,'
      '  reference,'
      '  id_customer,'
      '  name_customer,'
      '  email_customer,'
      '  address_customer,'
      '  city_customer,'
      '  postal_code_customer,'
      '  label,'
      '  creation_date,'
      '  id_creator,'
      '  nom_creator'
      'from'
      '  jtb_invoice')
    Left = 272
    Top = 16
    object FD_JTB_INVOICEID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
    end
    object FD_JTB_INVOICEDIRECTORY_NUMBER: TIntegerField
      FieldName = 'DIRECTORY_NUMBER'
      Origin = 'DIRECTORY_NUMBER'
    end
    object FD_JTB_INVOICEREFERENCE: TStringField
      FieldName = 'REFERENCE'
      Origin = 'REFERENCE'
      Size = 12
    end
    object FD_JTB_INVOICEID_CUSTOMER: TIntegerField
      FieldName = 'ID_CUSTOMER'
      Origin = 'ID_CUSTOMER'
    end
    object FD_JTB_INVOICENAME_CUSTOMER: TStringField
      FieldName = 'NAME_CUSTOMER'
      Origin = 'NAME_CUSTOMER'
      Size = 50
    end
    object FD_JTB_INVOICEEMAIL_CUSTOMER: TStringField
      FieldName = 'EMAIL_CUSTOMER'
      Origin = 'EMAIL_CUSTOMER'
      Size = 50
    end
    object FD_JTB_INVOICEADDRESS_CUSTOMER: TStringField
      FieldName = 'ADDRESS_CUSTOMER'
      Origin = 'ADDRESS_CUSTOMER'
      Size = 50
    end
    object FD_JTB_INVOICECITY_CUSTOMER: TStringField
      FieldName = 'CITY_CUSTOMER'
      Origin = 'CITY_CUSTOMER'
      Size = 50
    end
    object FD_JTB_INVOICEPOSTAL_CODE_CUSTOMER: TIntegerField
      FieldName = 'POSTAL_CODE_CUSTOMER'
      Origin = 'POSTAL_CODE_CUSTOMER'
    end
    object FD_JTB_INVOICELABEL: TStringField
      FieldName = 'LABEL'
      Origin = 'LABEL'
      Size = 50
    end
    object FD_JTB_INVOICECREATION_DATE: TDateField
      FieldName = 'CREATION_DATE'
      Origin = 'CREATION_DATE'
    end
    object FD_JTB_INVOICEID_CREATOR: TIntegerField
      FieldName = 'ID_CREATOR'
      Origin = 'ID_CREATOR'
    end
    object FD_JTB_INVOICENOM_CREATOR: TStringField
      FieldName = 'NOM_CREATOR'
      Origin = 'NOM_CREATOR'
      Size = 101
    end
  end
end
