unit EMPDevisUser;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, EMPDevisFrame, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, BDLSkin,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxLayoutContainer, cxClasses, dxLayoutControl, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, dxLayoutControlAdapters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, dxDateRanges, dxScrollbarAnnotations,
  Data.DB, cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxLayoutcxEditAdapters,
  cxContainer, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxDBEdit, cxCalendar, System.Actions,
  Vcl.ActnList;

type
  TfrmUser = class(TfrmFrame)
    LayoutControlGroup_Root: TdxLayoutGroup;
    LayoutControl: TdxLayoutControl;
    LayoutItem_btn_Refresh: TdxLayoutItem;
    LayoutItem_btn_Add: TdxLayoutItem;
    LayoutItem_btn_Update: TdxLayoutItem;
    LayoutItem_btn_Option: TdxLayoutItem;
    LayoutSeparatorItem1: TdxLayoutSeparatorItem;
    LayoutSeparatorItem2: TdxLayoutSeparatorItem;
    LayoutGroup_Navigation: TdxLayoutGroup;
    LayoutGroup_Body: TdxLayoutGroup;
    LayoutGroup_grd_User: TdxLayoutGroup;
    LayoutGroup_data_user: TdxLayoutGroup;
    LayoutGroup_grd_pdf: TdxLayoutGroup;
    LayoutItem_grd_User: TdxLayoutItem;
    LayoutItem_btn_ImportPDF: TdxLayoutItem;
    LayoutItem_grd_PDF: TdxLayoutItem;
    dxLayoutItem2: TdxLayoutItem;
    btn_Refresh: TcxButton;
    btn_Add: TcxButton;
    btn_Save: TcxButton;
    btn_Configuration: TcxButton;
    dxLayoutSeparatorItem1: TdxLayoutSeparatorItem;
    grd_UserDBTableView: TcxGridDBTableView;
    grd_UserLevel: TcxGridLevel;
    grd_User: TcxGrid;
    grd_PDFDBTableView: TcxGridDBTableView;
    grd_PDFLevel: TcxGridLevel;
    grd_PDF: TcxGrid;
    btn_PDF: TcxButton;
    LayoutGroup_Body_Informations: TdxLayoutGroup;
    LayoutGroup_Information_Pro: TdxLayoutGroup;
    LayoutGroup_Creation_Per: TdxLayoutGroup;
    LayoutGroup_Creation: TdxLayoutGroup;
    LayoutGroup_Personal: TdxLayoutGroup;
    LayoutItem_txtedt_Name: TdxLayoutItem;
    LayoutItem_txtedt_FistName: TdxLayoutItem;
    LayoutItem_txtedt_Fixe: TdxLayoutItem;
    LayoutItem_txtedt_Smartphone: TdxLayoutItem;
    LayoutItem_lkpcmb_Job: TdxLayoutItem;
    LayoutItem_txtedt_Email: TdxLayoutItem;
    LayoutGroup_InfoPro_Line1: TdxLayoutGroup;
    LayoutGroup_InfoPro_Line2: TdxLayoutGroup;
    LayoutItem_lkpcmb_Creator: TdxLayoutItem;
    LayoutItem_datedt_DateCreation: TdxLayoutItem;
    LayoutItem_txtedt_MailPerso: TdxLayoutItem;
    LayoutItem_txtedt_NumPerso: TdxLayoutItem;
    txtedt_Name: TcxDBTextEdit;
    txtedt_Firstname: TcxDBTextEdit;
    txtedt_Email: TcxDBTextEdit;
    txtedt_Phone: TcxDBTextEdit;
    txtedt_Smatphone: TcxDBTextEdit;
    DS_USER: TDataSource;
    grd_UserDBTableViewID: TcxGridDBColumn;
    grd_UserDBTableViewNAME: TcxGridDBColumn;
    grd_UserDBTableViewFIRSTNAME: TcxGridDBColumn;
    grd_UserDBTableViewSMARTPHONE: TcxGridDBColumn;
    grd_UserDBTableViewPHONE: TcxGridDBColumn;
    grd_UserDBTableViewEMAIL: TcxGridDBColumn;
    grd_UserDBTableViewFULLNAME: TcxGridDBColumn;
    grd_UserDBTableViewID_JOB: TcxGridDBColumn;
    datedt_DateCreation: TcxDBDateEdit;
    lkpcmb_Createur: TcxDBLookupComboBox;
    grd_UserDBTableViewID_CREATOR: TcxGridDBColumn;
    grd_UserDBTableViewDATE_CREATION: TcxGridDBColumn;
    DS_CREATEUR: TDataSource;
    txtedt_PersonalPhone: TcxDBTextEdit;
    txtedt_PersonalEmail: TcxDBTextEdit;
    grd_UserDBTableViewPERSONAL_SMARTPHONE: TcxGridDBColumn;
    grd_UserDBTableViewPERSONAL_EMAIL: TcxGridDBColumn;
    ActionList_Navigation: TActionList;
    Action_Refresh: TAction;
    Action_Add: TAction;
    Action_Save: TAction;
    Action_Configuration: TAction;
    lkpcmb_Job: TcxDBLookupComboBox;
    DS_JOB: TDataSource;
    procedure Action_RefreshExecute(Sender: TObject);
    procedure Action_AddExecute(Sender: TObject);
    procedure Action_SaveExecute(Sender: TObject);
    procedure Action_ConfigurationExecute(Sender: TObject);
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    constructor Create(AOwner: TComponent); override;
  end;

var
  frmUser: TfrmUser;

implementation

{$R *.dfm}

uses
  EMPDevisData, EMPDevisUserConfiguration;

procedure TfrmUser.Action_AddExecute(Sender: TObject);
var
  id_user : integer;
begin
  inherited;
  try
    id_user := frmData.UserAdd(IntToStr(frmData.id_user));
    frmData.FDMemGetTables('FD_USER');
    frmData.FD_USER.Locate('ID', id_user, [loCaseInsensitive]);

    ShowMessage('Nouvel utilisateur cr��.');
  except on E: Exception do
    ShowMessage('Une erreur est survenue lors de la cr�ation d''un utilisateur.');
  end;
end;

procedure TfrmUser.Action_ConfigurationExecute(Sender: TObject);
begin
  inherited;
  CreateFormUserConfiguration();
end;

procedure TfrmUser.Action_RefreshExecute(Sender: TObject);
begin
  inherited;
  frmdata.FDMemGetTables('FD_USER');
end;

procedure TfrmUser.Action_SaveExecute(Sender: TObject);
begin
  inherited;
  if (frmData.FD_USER.State = dsEdit) or (frmData.FD_USER.State = dsInsert) then
  begin
    try
      frmData.FD_USER.Edit;
      frmData.FD_USERID_MODIFIER.AsInteger := frmData.id_user;
      frmData.FD_USERDATE_MODIFICATION.AsDateTime := date();
      frmData.FD_USER.Post;
      frmData.FDMemUpdate('FD_USER');

      ShowMessage('L''Utilisateur ' + txtedt_Name.Text + ' ' + txtedt_Firstname.Text + ' a �t� sauvegard�.');
    except on E: Exception do
      Showmessage('Une erreur est survenue lors de l''enregistrement de l''utilisateur ' +
                    txtedt_Name.Text + ' ' + txtedt_Firstname.Text + '.');
    end;

  end
  else
  begin
    ShowMessage('Aucunes modifications apport�es.');
  end;
end;

Constructor TfrmUser.Create(AOwner: TComponent);
begin
  inherited;
  frmData.FDMemGetTables('FD_CREATEUR;FD_JOB');
end;

initialization
RegisterFrame(IDUser, TfrmUser);

end.
