object frmUserConfiguration: TfrmUserConfiguration
  Left = 0
  Top = 0
  Caption = 'Configuration'
  ClientHeight = 402
  ClientWidth = 501
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 501
    Height = 402
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = frmData.LayoutSkinLookAndFeel
    ExplicitLeft = 112
    ExplicitTop = 96
    ExplicitWidth = 300
    ExplicitHeight = 250
    object grd_Fonction: TcxGrid
      Left = 22
      Top = 52
      Width = 457
      Height = 328
      TabOrder = 0
      object grd_FonctionDBTableView: TcxGridDBTableView
        Navigator.Buttons.OnButtonClick = grd_FonctionDBTableViewNavigatorButtonsButtonClick
        Navigator.Buttons.CustomButtons = <
          item
            Hint = 'Add'
            ImageIndex = 0
          end
          item
            Hint = 'Delete'
            ImageIndex = 1
          end
          item
            Hint = 'Update'
            ImageIndex = 90
          end
          item
            Hint = 'Cancel'
            ImageIndex = 64
          end
          item
            Hint = 'Refresh'
            ImageIndex = 4
          end>
        Navigator.Buttons.Images = frmData.ImageList_24x24
        Navigator.Buttons.First.Visible = False
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.Visible = False
        Navigator.Buttons.Next.Visible = False
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.Visible = False
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.Visible = False
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.Visible = False
        Navigator.Visible = True
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DS_JOB
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.ColumnAutoWidth = True
        object grd_FonctionDBTableViewID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_FonctionDBTableViewLABEL: TcxGridDBColumn
          Caption = 'Fonction'
          DataBinding.FieldName = 'LABEL'
          HeaderAlignmentHorz = taCenter
        end
      end
      object grd_FonctionLevel: TcxGridLevel
        GridView = grd_FonctionDBTableView
      end
    end
    object LayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ShowBorder = False
      Index = -1
    end
    object LayoutGroup_Configuration: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      AlignHorz = ahClient
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_grd_Fonction: TdxLayoutItem
      Parent = LayoutGroup_Configuration
      AlignVert = avClient
      CaptionOptions.Text = 'Fonction'
      CaptionOptions.Visible = False
      Control = grd_Fonction
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object DS_JOB: TDataSource
    DataSet = frmData.FD_JOB
    Left = 40
    Top = 96
  end
end
