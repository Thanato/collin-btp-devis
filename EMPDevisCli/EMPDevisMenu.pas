unit EMPDevisMenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, BDLSkin,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  dxCustomTileControl, cxClasses, dxTileControl;

type
  TfrmMenu = class(TForm)
    TileControl: TdxTileControl;
    TileControlGroup_Gestion: TdxTileControlGroup;
    TileControlItem_Devis: TdxTileControlItem;
    TileControlItem_Facture: TdxTileControlItem;
    TileControlGroup_Intervenant: TdxTileControlGroup;
    TileControlItem_Customer: TdxTileControlItem;
    TileControlItem_WorkSupervisor: TdxTileControlItem;
    TileControlItem_Architect: TdxTileControlItem;
    TileControlItem_User: TdxTileControlItem;
    TileControlGroup_Configuration: TdxTileControlGroup;
    TileControlItem_OptionDevis: TdxTileControlItem;
    TileControlItem_OptionTaux: TdxTileControlItem;
    TileControlItem_Materials: TdxTileControlItem;
    TileControlItem_BuildingSite: TdxTileControlItem;

    procedure ActivateDetail(Sender: TdxTileControlItem);
  private
    { Déclarations privées }
  public
    { Déclarations publiques }
  end;

var
  frmMenu: TfrmMenu;

implementation

Uses
  EMPDevisFrame, EMPDevisData;

{$R *.dfm}

// Permet d'activer une frame
procedure TFrmMenu.ActivateDetail(Sender: TdxTileControlItem);
begin
  //frmData.ActiveOnglet := Sender.Tag;
  if Sender.DetailOptions.DetailControl = nil then
    Sender.DetailOptions.DetailControl := GetDetailControlClass(Sender.Tag).Create(Self);
end;

end.
