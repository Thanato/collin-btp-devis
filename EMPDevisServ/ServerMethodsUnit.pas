unit ServerMethodsUnit;

interface

uses System.SysUtils, System.Classes, System.Json,
    Datasnap.DSServer, Datasnap.DSAuth, FireDAC.Phys.FBDef, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.Client, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Stan.StorageBin, FireDAC.Comp.UI,
  FireDAC.Phys.IBBase, Data.FireDACJSONReflect;

type
{$METHODINFO ON}
  TServerMethods = class(TDataModule)
    FDPhysFBDriverLink: TFDPhysFBDriverLink;
    FD_Connection: TFDConnection;
    FDGUIxWaitCursor: TFDGUIxWaitCursor;
    FDStanStorageBinLink: TFDStanStorageBinLink;
    FDStoredProc: TFDStoredProc;
    FDUpdateORGA: TFDUpdateSQL;
    FD_INVOICE: TFDQuery;
    FD_INVOICEID: TIntegerField;
    FD_INVOICEDIRECTORY_NUMBER: TIntegerField;
    FD_INVOICEREFERENCE: TStringField;
    FD_INVOICEID_CUSTOMER: TIntegerField;
    FD_INVOICEID_BUILDING_SITE: TIntegerField;
    FD_INVOICELABEL: TStringField;
    FD_INVOICECREATION_DATE: TDateField;
    FD_INVOICEMODIFICATION_DATE: TDateField;
    FD_INVOICEID_CREATOR: TIntegerField;
    FD_INVOICEID_MODIFIER: TIntegerField;
    FD_INVOICEID_STATE: TIntegerField;
    FD_INVOICEID_CATEGORY: TIntegerField;
    FD_INVOICEID_TVA: TIntegerField;
    FD_INVOICETTC_AMOUNT: TFMTBCDField;
    FD_INVOICEHT_AMOUNT: TFMTBCDField;
    FD_INVOICEID_HOLDBACK: TIntegerField;
    FD_CUSTOMER: TFDQuery;
    FD_USER: TFDQuery;
    FD_CUSTOMERID: TIntegerField;
    FD_CUSTOMERNAME: TStringField;
    FD_CUSTOMERPRIVATE: TIntegerField;
    FD_CUSTOMERCONDITION: TStringField;
    FD_CUSTOMERPHONE: TStringField;
    FD_CUSTOMERSMARTPHONE: TStringField;
    FD_CUSTOMEREMAIL: TStringField;
    FD_CUSTOMERADDRESS: TStringField;
    FD_CUSTOMERPOSTAL_CODE: TIntegerField;
    FD_CUSTOMERCITY: TStringField;
    FD_USERID: TIntegerField;
    FD_USERNAME: TStringField;
    FD_USERFIRSTNAME: TStringField;
    FD_USERSMARTPHONE: TStringField;
    FD_USERPHONE: TStringField;
    FD_USEREMAIL: TStringField;
    FD_USERID_JOB: TIntegerField;
    FD_USERFULLNAME: TStringField;
    FD_INVOICEID_INVOICE_ASSOCIATE: TIntegerField;
    FD_CUSTOMERCONTACT: TStringField;
    FD_CREATEUR: TFDQuery;
    FD_CREATEURID: TIntegerField;
    FD_CREATEURFULLNAME: TStringField;
    FD_USERID_CREATOR: TIntegerField;
    FD_USERDATE_CREATION: TDateField;
    FD_USERPERSONAL_SMARTPHONE: TStringField;
    FD_USERPERSONAL_EMAIL: TStringField;
    FD_JOB: TFDQuery;
    FD_JOBID: TIntegerField;
    FD_JOBLABEL: TStringField;
    FD_USERID_MODIFIER: TIntegerField;
    FD_USERDATE_MODIFICATION: TDateField;
    FD_CUSTOMERID_CREATOR: TIntegerField;
    FD_CUSTOMERDATE_CREATION: TDateField;
    FD_CUSTOMERID_MODIFIER: TIntegerField;
    FD_CUSTOMERDATE_MODIFICATION: TDateField;
    FD_JTB_INVOICE: TFDQuery;
    FD_JTB_INVOICEID: TIntegerField;
    FD_JTB_INVOICEDIRECTORY_NUMBER: TIntegerField;
    FD_JTB_INVOICEREFERENCE: TStringField;
    FD_JTB_INVOICEID_CUSTOMER: TIntegerField;
    FD_JTB_INVOICENAME_CUSTOMER: TStringField;
    FD_JTB_INVOICEEMAIL_CUSTOMER: TStringField;
    FD_JTB_INVOICEADDRESS_CUSTOMER: TStringField;
    FD_JTB_INVOICECITY_CUSTOMER: TStringField;
    FD_JTB_INVOICEPOSTAL_CODE_CUSTOMER: TIntegerField;
    FD_JTB_INVOICELABEL: TStringField;
    FD_JTB_INVOICECREATION_DATE: TDateField;
    FD_JTB_INVOICEID_CREATOR: TIntegerField;
    FD_JTB_INVOICENOM_CREATOR: TStringField;
  private
    { D�clarations priv�es }
  public
    { D�clarations publiques }
    function EchoString(Value: string): string;
    function ReverseString(Value: string): string;
    function GetTables(TablesList, ParamsList: string): TFDJSONDataSets;
    function UpdTables(TablesList: string; ADeltaList: TFDJSONDeltas): Boolean;
    function ExecProc(ProcName, ParamsList, ValuesList, ResultsList: string): String;
  end;
{$METHODINFO OFF}

implementation


{$R *.dfm}


uses System.StrUtils;

function TServerMethods.EchoString(Value: string): string;
begin
  Result := Value;
end;


function TServerMethods.ReverseString(Value: string): string;
begin
  Result := System.StrUtils.ReverseString(Value);
end;

// Lit la/les table(s) pass�e(s) en param�tre
function TServerMethods.GetTables(TablesList, ParamsList: string): TFDJSONDataSets;
var
  Tablelocal: TFDQuery;
  ListTable, ListParams: TStringList;
  i: integer;
begin
  ListTable := TStringList.Create;
  ListTable.Delimiter := ';';
  ListTable.DelimitedText := TablesList;
  ListParams := TStringList.Create;
  ListParams.Delimiter := ';';
  ListParams.DelimitedText := ParamsList;
  Result := TFDJSONDataSets.Create;
  for i := 0 to ListTable.Count - 1 do
  begin
    try
      Tablelocal := ((FindComponent(ListTable[i])) as TFDQuery);
      Tablelocal.Active := False;
      if ListParams[0] <> '*' then
        Tablelocal.Params[0].AsString := ListParams[i];
      TFDJSONDataSetsWriter.ListAdd(Result, ListTable[i], Tablelocal);
    except
    end;
  end;
end;

// Met � jour la/les table(s) pass�e(s) en param�tre
function TServerMethods.UpdTables(TablesList: string; ADeltaList: TFDJSONDeltas): Boolean;
var
  LApply: IFDJSONDeltasApplyUpdates;
  Tablelocal: TFDQuery;
  ListTable: TStringList;
  i: integer;
begin
  LApply := TFDJSONDeltasApplyUpdates.Create(ADeltaList);
  ListTable := TStringList.Create;
  ListTable.Delimiter := ';';
  ListTable.DelimitedText := TablesList;
  for i := 0 to ListTable.Count - 1 do
  begin
    Tablelocal := ((FindComponent(ListTable[i])) as TFDQuery);
    LApply.ApplyUpdates(Tablelocal.Name, Tablelocal.Command);
    // Raise an exception if any errors.
    if LApply.Errors.Count > 0 then
      raise Exception.Create(LApply.Errors.Strings.Text);
  end;
  Result := True;
end;

// Execute la proc�dure pass�e en param�tre
function TServerMethods.ExecProc(ProcName, ParamsList, ValuesList, ResultsList: string): String;
var
  ListParams, ListValue, ListResult: TStringList;
  i, j: integer;
begin
  ListParams := TStringList.Create;
  ListValue := TStringList.Create;
  ListResult := TStringList.Create;
  try
    ListParams.Delimiter := ';';
    ListParams.StrictDelimiter := True;
    ListParams.DelimitedText := ParamsList;
    ListValue.Delimiter := ';';
    ListValue.StrictDelimiter := True;
    ListValue.DelimitedText := ValuesList;
    ListResult.Delimiter := ';';
    ListResult.StrictDelimiter := True;
    ListResult.DelimitedText := ResultsList;
    with FDStoredProc do
    begin
      Close;
      StoredProcName := ProcName;
      Prepare;
      try
        for i := 0 to ListParams.Count - 1 do
        begin
          ParamByName(ListParams[i]).AsString := ListValue[i];
        end;
        ExecProc;
        for j := 0 to ListResult.Count - 1 do
        begin
          if j = 0 then
            Result := Result + ParamByName(ListResult[j]).AsString
          else
            Result := Result + ';' + ParamByName(ListResult[j]).AsString;
        end;
      finally
        Unprepare;
      end;
    end;
  finally
    ListParams.Free;
    ListValue.Free;
    ListResult.Free;
  end;
end;

end.

