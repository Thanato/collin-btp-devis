unit EMPDevisInvoiceBilan;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinBasic, BDLSkin, System.DateUtils,
  dxSkinOffice2019Black, dxSkinOffice2019DarkGray, dxSkinOffice2019White,
  cxClasses, dxLayoutContainer, dxLayoutControl, cxContainer, cxEdit,
  Vcl.ComCtrls, dxCore, cxDateUtils, Data.DB, cxDropDownEdit, cxCalendar,
  cxTextEdit, cxMaskEdit, cxDBEdit, dxLayoutcxEditAdapters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, dxDateRanges,
  dxScrollbarAnnotations, cxDBData, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  dxBarBuiltInMenu, cxGridCustomPopupMenu, cxGridPopupMenu;

type
  TfrmInvoiceBilan = class(TForm)
    LayoutControlGroup_Root: TdxLayoutGroup;
    LayoutControl: TdxLayoutControl;
    LayoutGroup_Navigation: TdxLayoutGroup;
    LayoutGroup_BodyGrid: TdxLayoutGroup;
    LayoutItem_cmb_User: TdxLayoutItem;
    LayoutItem_datedt_Debut: TdxLayoutItem;
    LayoutItem_datedt_Fin: TdxLayoutItem;
    LayoutSeparatorItem1: TdxLayoutSeparatorItem;
    LayoutItem_grd_Bilan: TdxLayoutItem;
    datedt_Debut: TcxDateEdit;
    datedt_Fin: TcxDateEdit;
    cmb_User: TcxComboBox;
    grd_JTB_DevisDBTableView: TcxGridDBTableView;
    grd_JTB_DevisLevel: TcxGridLevel;
    grd_JTB_Devis: TcxGrid;
    DS_JTB_INVOICE: TDataSource;
    grd_JTB_DevisDBTableViewID: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewDIRECTORY_NUMBER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewREFERENCE: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewID_CUSTOMER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewNAME_CUSTOMER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewEMAIL_CUSTOMER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewADDRESS_CUSTOMER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewCITY_CUSTOMER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewPOSTAL_CODE_CUSTOMER: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewLABEL: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewCREATION_DATE: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewID_CREATOR: TcxGridDBColumn;
    grd_JTB_DevisDBTableViewNOM_CREATOR: TcxGridDBColumn;
    GridPopupMenu: TcxGridPopupMenu;
    procedure cmb_UserPropertiesEditValueChanged(Sender: TObject);
    procedure datedt_DebutPropertiesEditValueChanged(Sender: TObject);
    procedure datedt_FinPropertiesEditValueChanged(Sender: TObject);
  private
    { Déclarations privées }
    procedure Filter();
  public
    { Déclarations publiques }
  end;

var
  frmInvoiceBilan: TfrmInvoiceBilan;

  procedure CreationInvoiceBilan();

implementation

{$R *.dfm}

uses
  EMPDevisData;

procedure CreationInvoiceBilan();
begin
  frmInvoiceBilan := TfrmInvoiceBilan.Create(Application);
  try
    frmData.FDMemGetTables('FD_JTB_INVOICE');
    frmData.FD_JTB_INVOICE.Filtered := true;

    frmInvoiceBilan.datedt_Debut.Date :=  date() - (DayOf(date()) - 1 );
    frmInvoiceBilan.datedt_fin.Date := date();
    frmInvoiceBilan.cmb_User.ItemIndex := 0;

    frmData.FD_JTB_INVOICE.Filter := 'creation_date between ''' + frmInvoiceBilan.datedt_Debut.text + ''' and ''' +
                                    frmInvoiceBilan.datedt_Fin.text + '''';
    Showmessage(frmData.FD_JTB_INVOICE.Filter) ;
    frmInvoiceBilan.cmb_User.Clear;
    frmInvoiceBilan.cmb_User.Properties.Items.Add('Tous les utilisateurs');
    while not frmData.FD_USER.Eof do
    begin
      frmInvoiceBilan.cmb_User.Properties.Items.Add(frmData.FD_USERFULLNAME.AsString);

      frmData.FD_USER.Next;
    end;

    frmInvoiceBilan.ShowModal();
  finally
    FreeAndNil(frmInvoiceBilan);
  end;
end;

procedure TfrmInvoiceBilan.cmb_UserPropertiesEditValueChanged(Sender: TObject);
begin
  Filter()
end;

procedure TfrmInvoiceBilan.datedt_DebutPropertiesEditValueChanged(
  Sender: TObject);
begin
  Filter()
end;

procedure TfrmInvoiceBilan.datedt_FinPropertiesEditValueChanged(
  Sender: TObject);
begin
  Filter()
end;

procedure TfrmInvoiceBilan.Filter();
begin
  if cmb_user.Text = 'Tous les utilisateurs' then
  begin
    frmData.FD_JTB_INVOICE.Filter := 'creation_date between ''' + frmInvoiceBilan.datedt_Debut.text + ''' and ''' +
                                    frmInvoiceBilan.datedt_Fin.text + '''';
  end
  else
  begin
    frmData.FD_JTB_INVOICE.Filter := 'creation_date between ''' + frmInvoiceBilan.datedt_Debut.text + ''' and ''' +
                                    frmInvoiceBilan.datedt_Fin.text + ''' and nom_creator = ''' + cmb_User.Text + '''';
  end;
end;

end.
