//
// Cr�� par le g�n�rateur de proxy DataSnap.
// 03/10/2020 19:14:18
//

unit EMPDevisClientClassesUnit;

interface

uses System.JSON, Datasnap.DSProxyRest, Datasnap.DSClientRest, Data.DBXCommon, Data.DBXClient, Data.DBXDataSnap, Data.DBXJSON, Datasnap.DSProxy, System.Classes, System.SysUtils, Data.DB, Data.SqlExpr, Data.DBXDBReaders, Data.DBXCDSReaders, Data.FireDACJSONReflect, Data.DBXJSONReflect;

type

  IDSRestCachedTFDJSONDataSets = interface;

  TServerMethodsClient = class(TDSAdminRestClient)
  private
    FEchoStringCommand: TDSRestCommand;
    FReverseStringCommand: TDSRestCommand;
    FGetTablesCommand: TDSRestCommand;
    FGetTablesCommand_Cache: TDSRestCommand;
    FUpdTablesCommand: TDSRestCommand;
    FExecProcCommand: TDSRestCommand;
  public
    constructor Create(ARestConnection: TDSRestConnection); overload;
    constructor Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean); overload;
    destructor Destroy; override;
    function EchoString(Value: string; const ARequestFilter: string = ''): string;
    function ReverseString(Value: string; const ARequestFilter: string = ''): string;
    function GetTables(TablesList: string; ParamsList: string; const ARequestFilter: string = ''): TFDJSONDataSets;
    function GetTables_Cache(TablesList: string; ParamsList: string; const ARequestFilter: string = ''): IDSRestCachedTFDJSONDataSets;
    function UpdTables(TablesList: string; ADeltaList: TFDJSONDeltas; const ARequestFilter: string = ''): Boolean;
    function ExecProc(ProcName: string; ParamsList: string; ValuesList: string; ResultsList: string; const ARequestFilter: string = ''): string;
  end;

  IDSRestCachedTFDJSONDataSets = interface(IDSRestCachedObject<TFDJSONDataSets>)
  end;

  TDSRestCachedTFDJSONDataSets = class(TDSRestCachedObject<TFDJSONDataSets>, IDSRestCachedTFDJSONDataSets, IDSRestCachedCommand)
  end;

const
  TServerMethods_EchoString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods_ReverseString: array [0..1] of TDSRestParameterMetaData =
  (
    (Name: 'Value'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

  TServerMethods_GetTables: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'TablesList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ParamsList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 37; TypeName: 'TFDJSONDataSets')
  );

  TServerMethods_GetTables_Cache: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'TablesList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ParamsList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'String')
  );

  TServerMethods_UpdTables: array [0..2] of TDSRestParameterMetaData =
  (
    (Name: 'TablesList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ADeltaList'; Direction: 1; DBXType: 37; TypeName: 'TFDJSONDeltas'),
    (Name: ''; Direction: 4; DBXType: 4; TypeName: 'Boolean')
  );

  TServerMethods_ExecProc: array [0..4] of TDSRestParameterMetaData =
  (
    (Name: 'ProcName'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ParamsList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ValuesList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: 'ResultsList'; Direction: 1; DBXType: 26; TypeName: 'string'),
    (Name: ''; Direction: 4; DBXType: 26; TypeName: 'string')
  );

implementation

function TServerMethodsClient.EchoString(Value: string; const ARequestFilter: string): string;
begin
  if FEchoStringCommand = nil then
  begin
    FEchoStringCommand := FConnection.CreateCommand;
    FEchoStringCommand.RequestType := 'GET';
    FEchoStringCommand.Text := 'TServerMethods.EchoString';
    FEchoStringCommand.Prepare(TServerMethods_EchoString);
  end;
  FEchoStringCommand.Parameters[0].Value.SetWideString(Value);
  FEchoStringCommand.Execute(ARequestFilter);
  Result := FEchoStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodsClient.ReverseString(Value: string; const ARequestFilter: string): string;
begin
  if FReverseStringCommand = nil then
  begin
    FReverseStringCommand := FConnection.CreateCommand;
    FReverseStringCommand.RequestType := 'GET';
    FReverseStringCommand.Text := 'TServerMethods.ReverseString';
    FReverseStringCommand.Prepare(TServerMethods_ReverseString);
  end;
  FReverseStringCommand.Parameters[0].Value.SetWideString(Value);
  FReverseStringCommand.Execute(ARequestFilter);
  Result := FReverseStringCommand.Parameters[1].Value.GetWideString;
end;

function TServerMethodsClient.GetTables(TablesList: string; ParamsList: string; const ARequestFilter: string): TFDJSONDataSets;
begin
  if FGetTablesCommand = nil then
  begin
    FGetTablesCommand := FConnection.CreateCommand;
    FGetTablesCommand.RequestType := 'GET';
    FGetTablesCommand.Text := 'TServerMethods.GetTables';
    FGetTablesCommand.Prepare(TServerMethods_GetTables);
  end;
  FGetTablesCommand.Parameters[0].Value.SetWideString(TablesList);
  FGetTablesCommand.Parameters[1].Value.SetWideString(ParamsList);
  FGetTablesCommand.Execute(ARequestFilter);
  if not FGetTablesCommand.Parameters[2].Value.IsNull then
  begin
    FUnMarshal := TDSRestCommand(FGetTablesCommand.Parameters[2].ConnectionHandler).GetJSONUnMarshaler;
    try
      Result := TFDJSONDataSets(FUnMarshal.UnMarshal(FGetTablesCommand.Parameters[2].Value.GetJSONValue(True)));
      if FInstanceOwner then
        FGetTablesCommand.FreeOnExecute(Result);
    finally
      FreeAndNil(FUnMarshal)
    end
  end
  else
    Result := nil;
end;

function TServerMethodsClient.GetTables_Cache(TablesList: string; ParamsList: string; const ARequestFilter: string): IDSRestCachedTFDJSONDataSets;
begin
  if FGetTablesCommand_Cache = nil then
  begin
    FGetTablesCommand_Cache := FConnection.CreateCommand;
    FGetTablesCommand_Cache.RequestType := 'GET';
    FGetTablesCommand_Cache.Text := 'TServerMethods.GetTables';
    FGetTablesCommand_Cache.Prepare(TServerMethods_GetTables_Cache);
  end;
  FGetTablesCommand_Cache.Parameters[0].Value.SetWideString(TablesList);
  FGetTablesCommand_Cache.Parameters[1].Value.SetWideString(ParamsList);
  FGetTablesCommand_Cache.ExecuteCache(ARequestFilter);
  Result := TDSRestCachedTFDJSONDataSets.Create(FGetTablesCommand_Cache.Parameters[2].Value.GetString);
end;

function TServerMethodsClient.UpdTables(TablesList: string; ADeltaList: TFDJSONDeltas; const ARequestFilter: string): Boolean;
begin
  if FUpdTablesCommand = nil then
  begin
    FUpdTablesCommand := FConnection.CreateCommand;
    FUpdTablesCommand.RequestType := 'POST';
    FUpdTablesCommand.Text := 'TServerMethods."UpdTables"';
    FUpdTablesCommand.Prepare(TServerMethods_UpdTables);
  end;
  FUpdTablesCommand.Parameters[0].Value.SetWideString(TablesList);
  if not Assigned(ADeltaList) then
    FUpdTablesCommand.Parameters[1].Value.SetNull
  else
  begin
    FMarshal := TDSRestCommand(FUpdTablesCommand.Parameters[1].ConnectionHandler).GetJSONMarshaler;
    try
      FUpdTablesCommand.Parameters[1].Value.SetJSONValue(FMarshal.Marshal(ADeltaList), True);
      if FInstanceOwner then
        ADeltaList.Free
    finally
      FreeAndNil(FMarshal)
    end
    end;
  FUpdTablesCommand.Execute(ARequestFilter);
  Result := FUpdTablesCommand.Parameters[2].Value.GetBoolean;
end;

function TServerMethodsClient.ExecProc(ProcName: string; ParamsList: string; ValuesList: string; ResultsList: string; const ARequestFilter: string): string;
begin
  if FExecProcCommand = nil then
  begin
    FExecProcCommand := FConnection.CreateCommand;
    FExecProcCommand.RequestType := 'GET';
    FExecProcCommand.Text := 'TServerMethods.ExecProc';
    FExecProcCommand.Prepare(TServerMethods_ExecProc);
  end;
  FExecProcCommand.Parameters[0].Value.SetWideString(ProcName);
  FExecProcCommand.Parameters[1].Value.SetWideString(ParamsList);
  FExecProcCommand.Parameters[2].Value.SetWideString(ValuesList);
  FExecProcCommand.Parameters[3].Value.SetWideString(ResultsList);
  FExecProcCommand.Execute(ARequestFilter);
  Result := FExecProcCommand.Parameters[4].Value.GetWideString;
end;

constructor TServerMethodsClient.Create(ARestConnection: TDSRestConnection);
begin
  inherited Create(ARestConnection);
end;

constructor TServerMethodsClient.Create(ARestConnection: TDSRestConnection; AInstanceOwner: Boolean);
begin
  inherited Create(ARestConnection, AInstanceOwner);
end;

destructor TServerMethodsClient.Destroy;
begin
  FEchoStringCommand.DisposeOf;
  FReverseStringCommand.DisposeOf;
  FGetTablesCommand.DisposeOf;
  FGetTablesCommand_Cache.DisposeOf;
  FUpdTablesCommand.DisposeOf;
  FExecProcCommand.DisposeOf;
  inherited;
end;

end.

