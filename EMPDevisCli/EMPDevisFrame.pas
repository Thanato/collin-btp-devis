unit EMPDevisFrame;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls,
  Vcl.Forms, Vcl.Dialogs;

const
  IDFirst = 0;
  IDDevis = 1;
  IDUser = 2;
  IDCustomer = 3;
  IDLast = 4;

type
  TfrmFrame = class(TFrame)
  end;

  TfrmFrameClass = class of TfrmFrame;

function GetDetailControlClass(ATag: Integer): TfrmFrameClass;
procedure RegisterFrame(AID: Integer; AFrameClass: TfrmFrameClass);

implementation
{$R *.dfm}

var
  AFrameClasses: array [IDFirst .. IDLast] of TfrmFrameClass;

function GetDetailControlClass(ATag: Integer): TfrmFrameClass;
begin
  Result := AFrameClasses[ATag];
end;

procedure RegisterFrame(AID: Integer; AFrameClass: TfrmFrameClass);
begin
  AFrameClasses[AID] := AFrameClass;
end;

end.
