﻿inherited frmDevis: TfrmDevis
  Width = 1304
  Height = 769
  ExplicitWidth = 1304
  ExplicitHeight = 769
  object LayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 1304
    Height = 769
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = frmData.LayoutSkinLookAndFeel
    HighlightRoot = False
    object btn_ExportXLS: TcxButton
      Left = 617
      Top = 10
      Width = 97
      Height = 25
      Caption = 'Exporter Exel'
      OptionsImage.ImageIndex = 12
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 7
    end
    object btn_ExportPDF: TcxButton
      Left = 520
      Top = 10
      Width = 91
      Height = 25
      Caption = 'Exporter PDF'
      OptionsImage.ImageIndex = 13
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 6
    end
    object btn_Print: TcxButton
      Left = 439
      Top = 10
      Width = 75
      Height = 25
      Caption = 'Imprimer'
      OptionsImage.ImageIndex = 7
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 5
    end
    object btn_Update: TcxButton
      Left = 255
      Top = 10
      Width = 91
      Height = 25
      Action = Action_Update
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 3
    end
    object btn_Ajouter: TcxButton
      Left = 103
      Top = 10
      Width = 75
      Height = 25
      Action = Action_Add
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 1
    end
    object grd_Devis: TcxGrid
      Left = 10
      Top = 53
      Width = 325
      Height = 689
      TabOrder = 8
      object grd_DevisDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Buttons.Images = frmData.ImageList_24x24
        Navigator.Buttons.First.ImageIndex = 93
        Navigator.Buttons.PriorPage.Visible = False
        Navigator.Buttons.Prior.ImageIndex = 91
        Navigator.Buttons.Next.ImageIndex = 92
        Navigator.Buttons.NextPage.Visible = False
        Navigator.Buttons.Last.ImageIndex = 94
        Navigator.Buttons.Insert.Visible = False
        Navigator.Buttons.Delete.Visible = False
        Navigator.Buttons.Edit.Visible = False
        Navigator.Buttons.Post.Visible = False
        Navigator.Buttons.Cancel.Visible = False
        Navigator.Buttons.Refresh.ImageIndex = 4
        Navigator.Buttons.SaveBookmark.Visible = False
        Navigator.Buttons.GotoBookmark.Visible = False
        Navigator.Buttons.Filter.ImageIndex = 6
        Navigator.Visible = True
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.Layout = fplCompact
        FindPanel.Location = fplGroupByBox
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DS_INVOICE
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        object grd_DevisDBTableViewID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewDIRECTORY_NUMBER: TcxGridDBColumn
          Caption = 'N'#176' Dossier'
          DataBinding.FieldName = 'DIRECTORY_NUMBER'
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewREFERENCE: TcxGridDBColumn
          Caption = 'R'#233'f'#233'rence'
          DataBinding.FieldName = 'REFERENCE'
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_CUSTOMER: TcxGridDBColumn
          Caption = 'Client'
          DataBinding.FieldName = 'ID_CUSTOMER'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_BUILDING_SITE: TcxGridDBColumn
          Caption = 'Chantier'
          DataBinding.FieldName = 'ID_BUILDING_SITE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewLABEL: TcxGridDBColumn
          Caption = 'Libell'#233
          DataBinding.FieldName = 'LABEL'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewCREATION_DATE: TcxGridDBColumn
          Caption = 'Date Cr'#233'ation'
          DataBinding.FieldName = 'CREATION_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          HeaderAlignmentHorz = taCenter
          Width = 79
        end
        object grd_DevisDBTableViewMODIFICATION_DATE: TcxGridDBColumn
          Caption = 'Date Modification'
          DataBinding.FieldName = 'MODIFICATION_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_CREATOR: TcxGridDBColumn
          Caption = 'Cr'#233'ateur'
          DataBinding.FieldName = 'ID_CREATOR'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_MODIFIER: TcxGridDBColumn
          Caption = 'Modificateur'
          DataBinding.FieldName = 'ID_MODIFIER'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_STATE: TcxGridDBColumn
          Caption = 'Etat'
          DataBinding.FieldName = 'ID_STATE'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_CATEGORY: TcxGridDBColumn
          Caption = 'Cat'#233'gorie'
          DataBinding.FieldName = 'ID_CATEGORY'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_TVA: TcxGridDBColumn
          Caption = 'TVA'
          DataBinding.FieldName = 'ID_TVA'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewTTC_AMOUNT: TcxGridDBColumn
          Caption = 'Montant TTC'
          DataBinding.FieldName = 'TTC_AMOUNT'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewHT_AMOUNT: TcxGridDBColumn
          Caption = 'Montant HT'
          DataBinding.FieldName = 'HT_AMOUNT'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_DevisDBTableViewID_HOLDBACK: TcxGridDBColumn
          Caption = 'Retenue'
          DataBinding.FieldName = 'ID_HOLDBACK'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
      end
      object grd_DevisLevel: TcxGridLevel
        GridView = grd_DevisDBTableView
      end
    end
    object btn_ImportPDF: TcxButton
      Left = 1257
      Top = 96
      Width = 97
      Height = 25
      Caption = 'Importer PDF'
      OptionsImage.ImageIndex = 13
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 27
    end
    object grd_PDF: TcxGrid
      Left = 1257
      Top = 127
      Width = 295
      Height = 597
      TabOrder = 28
      object grd_PDFDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.Layout = fplCompact
        FindPanel.Location = fplGroupByBox
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
      end
      object grd_PDFLevel: TcxGridLevel
        GridView = grd_PDFDBTableView
      end
    end
    object btn_Bilan: TcxButton
      Left = 364
      Top = 10
      Width = 57
      Height = 25
      Action = ActionBilan
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 4
    end
    object btn_Refresh: TcxButton
      Left = 10
      Top = 10
      Width = 75
      Height = 25
      Action = Action_Refresh
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 0
    end
    object btn_Paste: TcxButton
      Left = 184
      Top = 10
      Width = 65
      Height = 25
      Action = Action_Paste
      OptionsImage.Images = frmData.ImageList_24x24
      SpeedButtonOptions.CanBeFocused = False
      SpeedButtonOptions.Transparent = True
      TabOrder = 2
    end
    object txtedt_Reference: TcxDBTextEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'REFERENCE'
      DataBinding.DataSource = DS_INVOICE
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 9
      Visible = False
      Width = 202
    end
    object datedt_DateCreation: TcxDBDateEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'CREATION_DATE'
      DataBinding.DataSource = DS_INVOICE
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 10
      Visible = False
      Width = 210
    end
    object txtedt_Libelle: TcxDBTextEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'LABEL'
      DataBinding.DataSource = DS_INVOICE
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 11
      Visible = False
      Width = 490
    end
    object lkpcmb_Etat: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'ID_STATE'
      DataBinding.DataSource = DS_INVOICE
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 13
      Visible = False
      Width = 145
    end
    object lkpcmb_TVA: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'ID_TVA'
      DataBinding.DataSource = DS_INVOICE
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 12
      Visible = False
      Width = 145
    end
    object lkpcmb_Categorie: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'ID_CATEGORY'
      DataBinding.DataSource = DS_INVOICE
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 14
      Visible = False
      Width = 111
    end
    object lkpcmb_Créateur: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'ID_CREATOR'
      DataBinding.DataSource = DS_INVOICE
      Enabled = False
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'FULLNAME'
        end>
      Properties.ListSource = DS_USER
      Properties.ReadOnly = True
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 24
      Visible = False
      Width = 145
    end
    object lkpcmb_Modifier: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'ID_MODIFIER'
      DataBinding.DataSource = DS_INVOICE
      Enabled = False
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'FULLNAME'
        end>
      Properties.ListSource = DS_USER
      Properties.ReadOnly = True
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 25
      Visible = False
      Width = 145
    end
    object datedt_Modification: TcxDBDateEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'MODIFICATION_DATE'
      DataBinding.DataSource = DS_INVOICE
      Enabled = False
      Properties.ReadOnly = True
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 26
      Visible = False
      Width = 145
    end
    object lkpcmb_AffaireAssocie: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 16
      Visible = False
      Width = 145
    end
    object lkpcmb_ConditionReglement: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      Enabled = False
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 19
      Visible = False
      Width = 253
    end
    object lkpcmb_Client: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'ID_CUSTOMER'
      DataBinding.DataSource = DS_INVOICE
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'NAME'
        end>
      Properties.ListSource = DS_CUSTOMER
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 18
      Visible = False
      Width = 145
    end
    object lkpcmb_Chantier: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      AutoSize = False
      DataBinding.DataField = 'ID_BUILDING_SITE'
      DataBinding.DataSource = DS_INVOICE
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 17
      Visible = False
      Height = 21
      Width = 145
    end
    object txtedt_Contact: TcxDBTextEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'CONTACT'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 20
      Visible = False
      Width = 145
    end
    object txtedt_TelFixe: TcxDBTextEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'PHONE'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 22
      Visible = False
      Width = 225
    end
    object txtedt_TelPortable: TcxDBTextEdit
      Left = 10000
      Top = 10000
      Enabled = False
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 23
      Visible = False
      Width = 156
    end
    object txtedt_Email: TcxDBTextEdit
      Left = 10000
      Top = 10000
      DataBinding.DataField = 'EMAIL'
      DataBinding.DataSource = DS_CUSTOMER
      Enabled = False
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 21
      Visible = False
      Width = 321
    end
    object lkpcmb_Dossier: TcxDBLookupComboBox
      Left = 10000
      Top = 10000
      Properties.ListColumns = <>
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 15
      Visible = False
      Width = 145
    end
    object LayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 2
      ShowBorder = False
      Index = -1
    end
    object LayoutGroup_Navigation: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Body: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutGroup_GridDevis: TdxLayoutGroup
      Parent = LayoutGroup_Body
      CaptionOptions.Text = 'New Group'
      SizeOptions.AssignedValues = [sovSizableHorz]
      SizeOptions.SizableHorz = True
      SizeOptions.Width = 325
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_BodyDevis: TdxLayoutGroup
      Parent = LayoutGroup_Body
      AlignHorz = ahClient
      CaptionOptions.Text = 'New Group'
      SizeOptions.AssignedValues = [sovSizableHorz]
      SizeOptions.SizableHorz = True
      SizeOptions.Width = 248
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 2
    end
    object LayoutGroup_GridPDF: TdxLayoutGroup
      Parent = LayoutGroup_Body
      CaptionOptions.Text = 'PDF'
      SizeOptions.AssignedValues = [sovSizableHorz]
      SizeOptions.SizableHorz = True
      SizeOptions.Width = 325
      ButtonOptions.Buttons = <>
      Index = 4
    end
    object LayoutItem_grd_Devis: TdxLayoutItem
      Parent = LayoutGroup_GridDevis
      AlignVert = avClient
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = grd_Devis
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_grd_PDF: TdxLayoutItem
      Parent = LayoutGroup_GridPDF
      AlignVert = avClient
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = grd_PDF
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 250
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutGroup_TabDevis: TdxLayoutGroup
      Parent = LayoutGroup_BodyDevis
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldTabbed
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Devis_CustomerFolder: TdxLayoutGroup
      Parent = LayoutGroup_TabDevis
      AlignHorz = ahCenter
      CaptionOptions.Text = 'Dossier Client'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 0
    end
    object LayoutGroup_Devis_Etude: TdxLayoutGroup
      Parent = LayoutGroup_TabDevis
      CaptionOptions.Text = 'Etude'
      ButtonOptions.Buttons = <>
      Index = 1
    end
    object LayoutGroup_PiedAffaire: TdxLayoutGroup
      Parent = LayoutGroup_TabDevis
      CaptionOptions.Text = 'Pied d'#39'Affaire'
      ButtonOptions.Buttons = <>
      Index = 2
    end
    object LayoutGroup_Debourse: TdxLayoutGroup
      Parent = LayoutGroup_TabDevis
      CaptionOptions.Text = 'D'#233'bours'#233's'
      ButtonOptions.Buttons = <>
      Index = 3
    end
    object LayoutSeparatorItem: TdxLayoutSeparatorItem
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutItem_btn_Add: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      AlignVert = avCenter
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Ajouter
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutItem_btn_Update: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      AlignVert = avCenter
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Update
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 91
      ControlOptions.ShowBorder = False
      Index = 4
    end
    object LayoutSeparatorItem1: TdxLayoutSeparatorItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Separator'
      Index = 5
    end
    object LayoutItem_btn_Printer: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      AlignVert = avCenter
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Print
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 8
    end
    object LayoutItem_btn_ExportPDF: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      AlignVert = avCenter
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_ExportPDF
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 91
      ControlOptions.ShowBorder = False
      Index = 9
    end
    object LayoutItem_btn_EportExel: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      AlignVert = avCenter
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_ExportXLS
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 97
      ControlOptions.ShowBorder = False
      Index = 10
    end
    object LayoutItem_btn_ImportPDF: TdxLayoutItem
      Parent = LayoutGroup_PDF_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_ImportPDF
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 97
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutGroup_PDF_Navigation: TdxLayoutGroup
      Parent = LayoutGroup_GridPDF
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_btn_Bilan: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Bilan
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 57
      ControlOptions.ShowBorder = False
      Index = 6
    end
    object LayoutSeparatorItem2: TdxLayoutSeparatorItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Separator'
      Index = 7
    end
    object LayoutSplitterItem1: TdxLayoutSplitterItem
      Parent = LayoutGroup_Body
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 1
    end
    object LayoutSplitterItem2: TdxLayoutSplitterItem
      Parent = LayoutGroup_Body
      SizeOptions.AssignedValues = [sovSizableHorz, sovSizableVert]
      SizeOptions.SizableHorz = False
      SizeOptions.SizableVert = False
      CaptionOptions.Text = 'Splitter'
      Index = 3
    end
    object LayoutGroup_Devis_DataDevis: TdxLayoutGroup
      Parent = LayoutGroup_Devis_CustomerFolder1
      CaptionOptions.Text = 'Informations'
      SizeOptions.AssignedValues = [sovSizableHorz]
      SizeOptions.SizableHorz = True
      SizeOptions.Width = 516
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 0
    end
    object LayoutGroup_Devis_DataLiaisons: TdxLayoutGroup
      Parent = LayoutGroup_Devis_CustomerFolder1
      CaptionOptions.Text = 'Liaisons'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 1
    end
    object LayoutGroup_Devis_CustomerFolder1: TdxLayoutGroup
      Parent = LayoutGroup_Devis_CustomerFolder
      AlignHorz = ahCenter
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_btn_Refresh: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Refresh
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 75
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_btn_Paste: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'New Item'
      CaptionOptions.Visible = False
      Control = btn_Paste
      ControlOptions.OriginalHeight = 25
      ControlOptions.OriginalWidth = 65
      ControlOptions.ShowBorder = False
      Index = 3
    end
    object dxLayoutSeparatorItem1: TdxLayoutSeparatorItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutGroup_Devis_CustomerFolder2: TdxLayoutGroup
      Parent = LayoutGroup_Devis_CustomerFolder
      AlignHorz = ahCenter
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_Reference: TdxLayoutItem
      Parent = LayoutGroup_FolderCustomer_Informations_line1
      AlignHorz = ahClient
      CaptionOptions.Text = 'R'#233'f'#233'rence'
      Control = txtedt_Reference
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_datedt_DateCreation: TdxLayoutItem
      Parent = LayoutGroup_FolderCustomer_Informations_line1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Date Cr'#233'ation'
      Control = datedt_DateCreation
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_txtedt_Libelle: TdxLayoutItem
      Parent = LayoutGroup_FolderCustomer_Informations_line2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Libell'#233
      Control = txtedt_Libelle
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_lkpcmb_TVA: TdxLayoutItem
      Parent = LayoutGroup_FolderCustomer_Informations_line3
      CaptionOptions.Text = 'TVA'
      Control = lkpcmb_TVA
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_lkpcmb_Etat: TdxLayoutItem
      Parent = LayoutGroup_FolderCustomer_Informations_line3
      CaptionOptions.Text = 'Etat'
      Control = lkpcmb_Etat
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_lkpcmb_Category: TdxLayoutItem
      Parent = LayoutGroup_FolderCustomer_Informations_line3
      CaptionOptions.Text = 'Cat'#233'gorie'
      Control = lkpcmb_Categorie
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 111
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutGroup_FolderCustomer_Informations_line3: TdxLayoutGroup
      Parent = LayoutGroup_Devis_DataDevis
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutGroup_FolderCustomer_Informations_line2: TdxLayoutGroup
      Parent = LayoutGroup_Devis_DataDevis
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
    object LayoutGroup_FolderCustomer_Informations_line1: TdxLayoutGroup
      Parent = LayoutGroup_Devis_DataDevis
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_Dossier: TdxLayoutItem
      Parent = LayoutGroup_Devis_DataLiaisons
      CaptionOptions.Text = 'Dossier'
      Control = lkpcmb_Dossier
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_txtedt_AffaireAssocié: TdxLayoutItem
      Parent = LayoutGroup_Devis_DataLiaisons
      CaptionOptions.Text = 'Affaire Associ'#233'e'
      Control = lkpcmb_AffaireAssocie
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_lkpcmb_Chantier: TdxLayoutItem
      Parent = LayoutGroup_Devis_DataLiaisons
      CaptionOptions.Text = 'Suivi Chantier'
      Control = lkpcmb_Chantier
      ControlOptions.OriginalHeight = 21
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutGroup_Devis_Client: TdxLayoutGroup
      Parent = LayoutGroup_Devis_CustomerFolder2
      CaptionOptions.Text = 'Client'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      Index = 0
    end
    object LayoutGroup_Devis_Createur: TdxLayoutGroup
      Parent = LayoutGroup_Devis_CustomerFolder2
      AlignHorz = ahClient
      CaptionOptions.Text = 'Utilisateur'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      Index = 1
    end
    object LayoutItem_lkpcmb_Customer: TdxLayoutItem
      Parent = LayoutGroup_Devis_Client_Line1
      CaptionOptions.Text = 'Client'
      Control = lkpcmb_Client
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_cmb_Condition: TdxLayoutItem
      Parent = LayoutGroup_Devis_Client_Line1
      AlignHorz = ahClient
      CaptionOptions.Text = 'Condition de r'#233'glement'
      Control = lkpcmb_ConditionReglement
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 253
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 1
    end
    object LayoutItem_txtedt_Portable: TdxLayoutItem
      Parent = LayoutGroup_Devis_Client_Line3
      AlignHorz = ahClient
      CaptionOptions.Text = 'T'#233'l'#233'phone Portable'
      Control = txtedt_TelPortable
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 1
    end
    object LayoutItem_txtedt_Fixe: TdxLayoutItem
      Parent = LayoutGroup_Devis_Client_Line3
      AlignHorz = ahClient
      CaptionOptions.Text = 'T'#233'l'#233'phone Fixe'
      Control = txtedt_TelFixe
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 184
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 0
    end
    object LayoutItem_txtedt_Contact: TdxLayoutItem
      Parent = LayoutGroup_Devis_Client_Line2
      CaptionOptions.Text = 'Contact'
      Control = txtedt_Contact
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 0
    end
    object LayoutItem_txtedt_Email: TdxLayoutItem
      Parent = LayoutGroup_Devis_Client_Line2
      CaptionOptions.Text = 'Email'
      Control = txtedt_Email
      ControlOptions.AutoControlAreaAlignment = False
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 321
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 1
    end
    object LayoutItem_lkpcmb_Creator: TdxLayoutItem
      Parent = LayoutGroup_Devis_Createur
      CaptionOptions.Text = 'Cr'#233'ateur'
      Control = lkpcmb_Créateur
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 0
    end
    object LayoutItem_lkpcmb_Modifier: TdxLayoutItem
      Parent = LayoutGroup_Devis_Createur
      CaptionOptions.Text = 'Modifieur'
      Control = lkpcmb_Modifier
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 145
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 1
    end
    object LayoutItem_datedt_ModificationDate: TdxLayoutItem
      Parent = LayoutGroup_Devis_Createur
      CaptionOptions.Text = 'Modification'
      Control = datedt_Modification
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Enabled = False
      Index = 2
    end
    object LayoutGroup_Devis_Client_Line1: TdxLayoutGroup
      Parent = LayoutGroup_Devis_Client
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_Devis_Client_Line3: TdxLayoutGroup
      Parent = LayoutGroup_Devis_Client
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 1
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 2
    end
    object LayoutGroup_Devis_Client_Line2: TdxLayoutGroup
      Parent = LayoutGroup_Devis_Client
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 1
    end
  end
  object cxDBTreeList1: TcxDBTreeList
    Left = 384
    Top = 112
    Width = 801
    Height = 612
    Bands = <>
    Navigator.Buttons.CustomButtons = <>
    RootValue = -1
    ScrollbarAnnotations.CustomAnnotations = <>
    TabOrder = 1
  end
  object DS_INVOICE: TDataSource
    DataSet = frmData.FD_INVOICE
    OnDataChange = DS_INVOICEDataChange
    Left = 48
    Top = 160
  end
  object DS_USER: TDataSource
    DataSet = frmData.FD_USER
    Left = 112
    Top = 160
  end
  object DS_CUSTOMER: TDataSource
    DataSet = frmData.FD_CUSTOMER
    Left = 80
    Top = 160
  end
  object ActionList_Navigation: TActionList
    Images = frmData.ImageList_24x24
    Left = 48
    Top = 216
    object Action_Refresh: TAction
      Caption = 'Rafraichir'
      ImageIndex = 4
      OnExecute = Action_RefreshExecute
    end
    object Action_Add: TAction
      Caption = 'Ajouter'
      ImageIndex = 0
      OnExecute = Action_AddExecute
    end
    object Action_Update: TAction
      Caption = 'Sauvegarder'
      ImageIndex = 73
      OnExecute = Action_UpdateExecute
    end
    object Action_Paste: TAction
      Caption = 'Copier'
      ImageIndex = 70
      OnExecute = Action_PasteExecute
    end
    object ActionBilan: TAction
      Caption = 'Bilan'
      ImageIndex = 15
      OnExecute = ActionBilanExecute
    end
  end
end
