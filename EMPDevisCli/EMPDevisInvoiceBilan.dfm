object frmInvoiceBilan: TfrmInvoiceBilan
  Left = 0
  Top = 0
  Caption = 'Bilan des devis'
  ClientHeight = 620
  ClientWidth = 986
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object LayoutControl: TdxLayoutControl
    Left = 0
    Top = 0
    Width = 986
    Height = 620
    Align = alClient
    TabOrder = 0
    LayoutLookAndFeel = frmData.LayoutSkinLookAndFeel
    object datedt_Debut: TcxDateEdit
      Left = 258
      Top = 10
      Properties.Alignment.Horz = taCenter
      Properties.OnEditValueChanged = datedt_DebutPropertiesEditValueChanged
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 1
      Width = 121
    end
    object datedt_Fin: TcxDateEdit
      Left = 402
      Top = 10
      Properties.Alignment.Horz = taCenter
      Properties.OnEditValueChanged = datedt_FinPropertiesEditValueChanged
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 2
      Width = 121
    end
    object cmb_User: TcxComboBox
      Left = 58
      Top = 10
      Properties.Alignment.Horz = taCenter
      Properties.OnEditValueChanged = cmb_UserPropertiesEditValueChanged
      Style.HotTrack = False
      Style.TransparentBorder = False
      TabOrder = 0
      Text = 'cmb_User'
      Width = 148
    end
    object grd_JTB_Devis: TcxGrid
      Left = 10
      Top = 47
      Width = 966
      Height = 563
      TabOrder = 3
      object grd_JTB_DevisDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FindPanel.DisplayMode = fpdmAlways
        FindPanel.Layout = fplCompact
        FindPanel.Location = fplGroupByBox
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = DS_JTB_INVOICE
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        object grd_JTB_DevisDBTableViewID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          HeaderAlignmentHorz = taCenter
          VisibleForCustomization = False
        end
        object grd_JTB_DevisDBTableViewDIRECTORY_NUMBER: TcxGridDBColumn
          Caption = 'N'#176' Dossier'
          DataBinding.FieldName = 'DIRECTORY_NUMBER'
          HeaderAlignmentHorz = taCenter
        end
        object grd_JTB_DevisDBTableViewREFERENCE: TcxGridDBColumn
          Caption = 'R'#233'f'#233'rence'
          DataBinding.FieldName = 'REFERENCE'
          HeaderAlignmentHorz = taCenter
        end
        object grd_JTB_DevisDBTableViewID_CUSTOMER: TcxGridDBColumn
          DataBinding.FieldName = 'ID_CUSTOMER'
          Visible = False
          VisibleForCustomization = False
        end
        object grd_JTB_DevisDBTableViewLABEL: TcxGridDBColumn
          Caption = 'Libell'#233
          DataBinding.FieldName = 'LABEL'
          HeaderAlignmentHorz = taCenter
        end
        object grd_JTB_DevisDBTableViewNAME_CUSTOMER: TcxGridDBColumn
          Caption = 'Client'
          DataBinding.FieldName = 'NAME_CUSTOMER'
          HeaderAlignmentHorz = taCenter
          Width = 179
        end
        object grd_JTB_DevisDBTableViewEMAIL_CUSTOMER: TcxGridDBColumn
          Caption = 'Email Client'
          DataBinding.FieldName = 'EMAIL_CUSTOMER'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_JTB_DevisDBTableViewADDRESS_CUSTOMER: TcxGridDBColumn
          Caption = 'Adresse Client'
          DataBinding.FieldName = 'ADDRESS_CUSTOMER'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_JTB_DevisDBTableViewCITY_CUSTOMER: TcxGridDBColumn
          Caption = 'Ville Client'
          DataBinding.FieldName = 'CITY_CUSTOMER'
          Visible = False
          HeaderAlignmentHorz = taCenter
        end
        object grd_JTB_DevisDBTableViewPOSTAL_CODE_CUSTOMER: TcxGridDBColumn
          Caption = 'Code Postal Client'
          DataBinding.FieldName = 'POSTAL_CODE_CUSTOMER'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Width = 116
        end
        object grd_JTB_DevisDBTableViewCREATION_DATE: TcxGridDBColumn
          Caption = 'Date de cr'#233'ation'
          DataBinding.FieldName = 'CREATION_DATE'
          PropertiesClassName = 'TcxDateEditProperties'
          HeaderAlignmentHorz = taCenter
          Width = 117
        end
        object grd_JTB_DevisDBTableViewID_CREATOR: TcxGridDBColumn
          DataBinding.FieldName = 'ID_CREATOR'
          Visible = False
        end
        object grd_JTB_DevisDBTableViewNOM_CREATOR: TcxGridDBColumn
          Caption = 'Cr'#233'ateur'
          DataBinding.FieldName = 'NOM_CREATOR'
          HeaderAlignmentHorz = taCenter
          Width = 247
        end
      end
      object grd_JTB_DevisLevel: TcxGridLevel
        GridView = grd_JTB_DevisDBTableView
      end
    end
    object LayoutControlGroup_Root: TdxLayoutGroup
      AlignHorz = ahClient
      AlignVert = avClient
      ButtonOptions.Buttons = <>
      Hidden = True
      ItemIndex = 2
      ShowBorder = False
      Index = -1
    end
    object LayoutGroup_Navigation: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ItemIndex = 2
      LayoutDirection = ldHorizontal
      ShowBorder = False
      Index = 0
    end
    object LayoutGroup_BodyGrid: TdxLayoutGroup
      Parent = LayoutControlGroup_Root
      AlignVert = avClient
      CaptionOptions.Text = 'New Group'
      ButtonOptions.Buttons = <>
      ShowBorder = False
      Index = 2
    end
    object LayoutItem_cmb_User: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Cr'#233'ateur'
      Control = cmb_User
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 148
      ControlOptions.ShowBorder = False
      Index = 0
    end
    object LayoutItem_datedt_Debut: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'Devis du'
      Control = datedt_Debut
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 1
    end
    object LayoutItem_datedt_Fin: TdxLayoutItem
      Parent = LayoutGroup_Navigation
      CaptionOptions.Text = 'au'
      Control = datedt_Fin
      ControlOptions.OriginalHeight = 19
      ControlOptions.OriginalWidth = 121
      ControlOptions.ShowBorder = False
      Index = 2
    end
    object LayoutSeparatorItem1: TdxLayoutSeparatorItem
      Parent = LayoutControlGroup_Root
      CaptionOptions.Text = 'Separator'
      Index = 1
    end
    object LayoutItem_grd_Bilan: TdxLayoutItem
      Parent = LayoutGroup_BodyGrid
      AlignVert = avClient
      CaptionOptions.Text = 'grd_JTB_Devis'
      CaptionOptions.Visible = False
      Control = grd_JTB_Devis
      ControlOptions.OriginalHeight = 200
      ControlOptions.OriginalWidth = 681
      ControlOptions.ShowBorder = False
      Index = 0
    end
  end
  object DS_JTB_INVOICE: TDataSource
    DataSet = frmData.FD_JTB_INVOICE
    Left = 64
    Top = 120
  end
  object GridPopupMenu: TcxGridPopupMenu
    Grid = grd_JTB_Devis
    PopupMenus = <>
    Left = 96
    Top = 120
  end
end
