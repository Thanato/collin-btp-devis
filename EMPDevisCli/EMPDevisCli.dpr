program EMPDevisCli;

uses
  Vcl.Forms,
  EMPDevisMenu in 'EMPDevisMenu.pas' {frmMenu},
  EMPDevisClientClassesUnit in 'EMPDevisClientClassesUnit.pas',
  EMPDevisClientModuleUnit in 'EMPDevisClientModuleUnit.pas' {ClientModule: TDataModule},
  EMPDevisData in 'EMPDevisData.pas' {frmData: TDataModule},
  EMPDevisFrame in 'EMPDevisFrame.pas' {frmFrame: TFrame},
  EMPDevisDevis in 'EMPDevisDevis.pas' {frmDevis: TFrame},
  EMPDevisUser in 'EMPDevisUser.pas' {frmUser: TFrame},
  EMPDevisUserConfiguration in 'EMPDevisUserConfiguration.pas' {frmUserConfiguration},
  EMPDevisCustomer in 'EMPDevisCustomer.pas' {frmCustomer: TFrame},
  EMPDevisInvoiceBilan in 'EMPDevisInvoiceBilan.pas' {frmInvoiceBilan};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmData, frmData);
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmDevis, frmDevis);
  Application.CreateForm(TfrmUser, frmUser);
  Application.CreateForm(TfrmInvoiceBilan, frmInvoiceBilan);
  // TODO : Fichier Localizer
  frmData.Initialisation();

  Application.Run;
end.
